﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using HealthDaimond;


namespace GenerateHD
{
    class Program
    {

        static void Main(string[] args)
        {

            string HDID = "";

            string consString = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;

            Console.WriteLine("who?");
            string FirstOwner = Console.ReadLine();
            Console.WriteLine("Count?");
            int HDCount = int.Parse(Console.ReadLine());


            try
            {
                var conStr = GenerateHD.Properties.Settings.Default.HealthTechConnectionString;

                using (var context = new HDDemoDataContext())
                {
                    HDID = (from e in context.HDiamond
                            orderby e.ID descending
                            select e.HDID).FirstOrDefault();

                }


            }
            catch (Exception e)
            {


            }





            //foreach (DataRow item in tempDt.Rows)
            //{
            //    Console.WriteLine(item["HDID"].ToString() + item["FirstOwnerID"].ToString() + item["CurrentOwnerID"].ToString() + item["GenerateDateTime"].ToString() + item["TransactionDateTime"].ToString() );
            //}

            //List<HDiamond> HDList = HDiamond.GetHDiamondDataTable(HDID, HDCount);

            //List<string> tempList = new List<string>();

            //foreach (var item in HDList)
            //{
            //    tempList.Add(item.HDID);
            //}

            //HDWallet AW = HDWallet.CreateHDWallet(FirstOwner);

            //Console.WriteLine(AW.IncreaseHD(tempList));

            

            List<HealthDaimond.HDiamond> HDList = HealthDaimond.HDiamond.GenerateHDiamond(HDID, FirstOwner, HDCount);

            DataTable tempDt = new DataTable();

            tempDt.Columns.Add("HDID");
            tempDt.Columns.Add("Owner");

            foreach (HealthDaimond.HDiamond item in HDList)
            {
                DataRow dr = tempDt.NewRow();
                dr["HDID"] = item.HDID;
                dr["Owner"] = item.Owner;

                tempDt.Rows.Add(dr);
            }

            if (tempDt.Rows.Count > 0)
            {

                using (SqlConnection con = new SqlConnection(consString))
                {
                    using (SqlCommand cmd = new SqlCommand("Insert_HDiamond"))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Connection = con;
                        cmd.Parameters.AddWithValue("@insHDiamondDt", tempDt);
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                }


            }//insert HD

            //foreach (DataRow item in tempDt.Rows)
            //{
            //    Console.WriteLine(item["HDID"] + " ");
            //}

            //HDKey keys = new HDKey();

            HDKey keypair = HDKey.GenerateRSAKeys(FirstOwner);

            string pubkey = keypair.publicKey;
            string prikey = keypair.privateKey;




            //using (SqlConnection con = new SqlConnection(consString))
            //{
            //    string cmdstr = @"INSERT [HealthTech].[dbo].[Keypair] (ID, PublicKey, PrivateKey)  
            //                      VALUES ('"+ FirstOwner +"','" + pubkey +"','"+ prikey +"') ";

            //    using (SqlCommand cmd = new SqlCommand())
            //    {
            //        cmd.CommandType = CommandType.Text;
            //        cmd.CommandText = cmdstr;
            //        cmd.Connection = con;
            //        con.Open();
            //        //cmd.ExecuteNonQuery();
            //        con.Close();
            //    }
            //}

            //string enc = keys.Encrypt(pubkey, FirstOwner);

            //string dec = keys.Decrypt(prikey, enc);

            //Console.WriteLine(pubkey);

            //Console.WriteLine();

            //Console.WriteLine(enc);

            //Console.WriteLine();

            //Console.WriteLine(prikey);

            //Console.WriteLine();

            //Console.WriteLine(dec);

            Console.ReadKey();

        }
    }


}
