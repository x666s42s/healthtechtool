﻿namespace HealthTechHistoryTool
{
    partial class HealthTool
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置受控資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.dataInsert = new System.Windows.Forms.TabPage();
            this.label14 = new System.Windows.Forms.Label();
            this.FirstVisitDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.SymptomText = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.LifestyleGroupBox = new System.Windows.Forms.GroupBox();
            this.WorkingComboBox = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.BowelHabitsComboBox = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.BetelNutCheckBox = new System.Windows.Forms.CheckBox();
            this.DrunkCheckBox = new System.Windows.Forms.CheckBox();
            this.SmokeCheckBox = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.SportsRadioButton1 = new System.Windows.Forms.RadioButton();
            this.SportsRadioButton2 = new System.Windows.Forms.RadioButton();
            this.label10 = new System.Windows.Forms.Label();
            this.BreakfastHabitComboBox = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.SleepHabitsComboBox = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.HealthInfoGroupBox = new System.Windows.Forms.GroupBox();
            this.cb18Text = new System.Windows.Forms.TextBox();
            this.cb17Text = new System.Windows.Forms.TextBox();
            this.cb16Text = new System.Windows.Forms.TextBox();
            this.cb14Text = new System.Windows.Forms.TextBox();
            this.cb13Text = new System.Windows.Forms.TextBox();
            this.checkBox18 = new System.Windows.Forms.CheckBox();
            this.checkBox17 = new System.Windows.Forms.CheckBox();
            this.checkBox16 = new System.Windows.Forms.CheckBox();
            this.checkBox15 = new System.Windows.Forms.CheckBox();
            this.checkBox14 = new System.Windows.Forms.CheckBox();
            this.checkBox13 = new System.Windows.Forms.CheckBox();
            this.checkBox12 = new System.Windows.Forms.CheckBox();
            this.checkBox11 = new System.Windows.Forms.CheckBox();
            this.checkBox10 = new System.Windows.Forms.CheckBox();
            this.checkBox9 = new System.Windows.Forms.CheckBox();
            this.checkBox8 = new System.Windows.Forms.CheckBox();
            this.checkBox7 = new System.Windows.Forms.CheckBox();
            this.checkBox6 = new System.Windows.Forms.CheckBox();
            this.checkBox5 = new System.Windows.Forms.CheckBox();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.PersonalInfoGroupBox = new System.Windows.Forms.GroupBox();
            this.BloodTypeComboBox = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.PatientAddress = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.PatientPhone = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.genderGroupBox = new System.Windows.Forms.GroupBox();
            this.maleRB = new System.Windows.Forms.RadioButton();
            this.femaleRB = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.PatientBirthday = new System.Windows.Forms.DateTimePicker();
            this.label8 = new System.Windows.Forms.Label();
            this.PatientName = new System.Windows.Forms.TextBox();
            this.PatientID = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.CallLoginBtn = new System.Windows.Forms.Button();
            this.AdviserName = new System.Windows.Forms.Label();
            this.InsertTreatment = new System.Windows.Forms.Button();
            this.singleSelect = new System.Windows.Forms.TabPage();
            this.ExportWord = new System.Windows.Forms.Button();
            this.preview = new System.Windows.Forms.GroupBox();
            this.RVAdvisorName = new System.Windows.Forms.Label();
            this.RVSymptom = new System.Windows.Forms.Label();
            this.RVWorking = new System.Windows.Forms.Label();
            this.RVSmokeDrunkBetelNut = new System.Windows.Forms.Label();
            this.RVHealthInfoResult = new System.Windows.Forms.Label();
            this.RVPatientBirthday = new System.Windows.Forms.Label();
            this.RVSmokeDrunkBetelNutlb = new System.Windows.Forms.Label();
            this.RVBowel = new System.Windows.Forms.Label();
            this.RVSport = new System.Windows.Forms.Label();
            this.RVBreakfast = new System.Windows.Forms.Label();
            this.RVSleep = new System.Windows.Forms.Label();
            this.RVPatientAddress = new System.Windows.Forms.Label();
            this.RVPatientPhone = new System.Windows.Forms.Label();
            this.RVPatientGender = new System.Windows.Forms.Label();
            this.RVBloodType = new System.Windows.Forms.Label();
            this.RVPatientID = new System.Windows.Forms.Label();
            this.RVPatientName = new System.Windows.Forms.Label();
            this.RVfirstvisitdate = new System.Windows.Forms.Label();
            this.RVSymptomlb = new System.Windows.Forms.Label();
            this.RVAdvisorNamelb = new System.Windows.Forms.Label();
            this.RVWorkinglb = new System.Windows.Forms.Label();
            this.RVBowellb = new System.Windows.Forms.Label();
            this.RVSportlb = new System.Windows.Forms.Label();
            this.RVBreakfastlb = new System.Windows.Forms.Label();
            this.RVSleeplb = new System.Windows.Forms.Label();
            this.RVHealthInfoResultlb = new System.Windows.Forms.Label();
            this.RVPatientAddresslb = new System.Windows.Forms.Label();
            this.RVBloodTypelb = new System.Windows.Forms.Label();
            this.RVPatientGenderlb = new System.Windows.Forms.Label();
            this.RVPatientBirthdaylb = new System.Windows.Forms.Label();
            this.RVPatientPhonelb = new System.Windows.Forms.Label();
            this.RVPatientIDlb = new System.Windows.Forms.Label();
            this.RVPatientNamelb = new System.Windows.Forms.Label();
            this.RVfirstvisitdatelb = new System.Windows.Forms.Label();
            this.patientlistbox = new System.Windows.Forms.ListBox();
            this.label15 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.dataInsert.SuspendLayout();
            this.LifestyleGroupBox.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.HealthInfoGroupBox.SuspendLayout();
            this.PersonalInfoGroupBox.SuspendLayout();
            this.genderGroupBox.SuspendLayout();
            this.singleSelect.SuspendLayout();
            this.preview.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.dataInsert);
            this.tabControl1.Controls.Add(this.singleSelect);
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(952, 565);
            this.tabControl1.TabIndex = 0;
            // 
            // dataInsert
            // 
            this.dataInsert.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dataInsert.Controls.Add(this.label14);
            this.dataInsert.Controls.Add(this.FirstVisitDateTimePicker);
            this.dataInsert.Controls.Add(this.SymptomText);
            this.dataInsert.Controls.Add(this.label13);
            this.dataInsert.Controls.Add(this.LifestyleGroupBox);
            this.dataInsert.Controls.Add(this.HealthInfoGroupBox);
            this.dataInsert.Controls.Add(this.PersonalInfoGroupBox);
            this.dataInsert.Controls.Add(this.CallLoginBtn);
            this.dataInsert.Controls.Add(this.AdviserName);
            this.dataInsert.Controls.Add(this.InsertTreatment);
            this.dataInsert.Location = new System.Drawing.Point(4, 25);
            this.dataInsert.Name = "dataInsert";
            this.dataInsert.Padding = new System.Windows.Forms.Padding(3);
            this.dataInsert.Size = new System.Drawing.Size(944, 536);
            this.dataInsert.TabIndex = 0;
            this.dataInsert.Text = "資料輸入";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(123, 15);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(67, 15);
            this.label14.TabIndex = 36;
            this.label14.Text = "初診日期";
            // 
            // FirstVisitDateTimePicker
            // 
            this.FirstVisitDateTimePicker.Location = new System.Drawing.Point(196, 8);
            this.FirstVisitDateTimePicker.Name = "FirstVisitDateTimePicker";
            this.FirstVisitDateTimePicker.Size = new System.Drawing.Size(200, 25);
            this.FirstVisitDateTimePicker.TabIndex = 35;
            // 
            // SymptomText
            // 
            this.SymptomText.Location = new System.Drawing.Point(55, 427);
            this.SymptomText.Multiline = true;
            this.SymptomText.Name = "SymptomText";
            this.SymptomText.Size = new System.Drawing.Size(701, 102);
            this.SymptomText.TabIndex = 34;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label13.Location = new System.Drawing.Point(9, 427);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(39, 15);
            this.label13.TabIndex = 33;
            this.label13.Text = "症狀";
            // 
            // LifestyleGroupBox
            // 
            this.LifestyleGroupBox.BackColor = System.Drawing.Color.Transparent;
            this.LifestyleGroupBox.Controls.Add(this.WorkingComboBox);
            this.LifestyleGroupBox.Controls.Add(this.label12);
            this.LifestyleGroupBox.Controls.Add(this.BowelHabitsComboBox);
            this.LifestyleGroupBox.Controls.Add(this.label11);
            this.LifestyleGroupBox.Controls.Add(this.BetelNutCheckBox);
            this.LifestyleGroupBox.Controls.Add(this.DrunkCheckBox);
            this.LifestyleGroupBox.Controls.Add(this.SmokeCheckBox);
            this.LifestyleGroupBox.Controls.Add(this.groupBox1);
            this.LifestyleGroupBox.Controls.Add(this.label10);
            this.LifestyleGroupBox.Controls.Add(this.BreakfastHabitComboBox);
            this.LifestyleGroupBox.Controls.Add(this.label7);
            this.LifestyleGroupBox.Controls.Add(this.SleepHabitsComboBox);
            this.LifestyleGroupBox.Controls.Add(this.label5);
            this.LifestyleGroupBox.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.LifestyleGroupBox.ForeColor = System.Drawing.SystemColors.ControlText;
            this.LifestyleGroupBox.Location = new System.Drawing.Point(655, 193);
            this.LifestyleGroupBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.LifestyleGroupBox.Name = "LifestyleGroupBox";
            this.LifestyleGroupBox.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.LifestyleGroupBox.Size = new System.Drawing.Size(282, 228);
            this.LifestyleGroupBox.TabIndex = 32;
            this.LifestyleGroupBox.TabStop = false;
            this.LifestyleGroupBox.Text = "生活習慣";
            // 
            // WorkingComboBox
            // 
            this.WorkingComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.WorkingComboBox.FormattingEnabled = true;
            this.WorkingComboBox.Items.AddRange(new object[] {
            "8小時以內",
            "8~12小時",
            "12小時以上"});
            this.WorkingComboBox.Location = new System.Drawing.Point(104, 190);
            this.WorkingComboBox.Name = "WorkingComboBox";
            this.WorkingComboBox.Size = new System.Drawing.Size(121, 23);
            this.WorkingComboBox.TabIndex = 36;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(30, 193);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(71, 15);
            this.label12.TabIndex = 35;
            this.label12.Text = "工作時間";
            // 
            // BowelHabitsComboBox
            // 
            this.BowelHabitsComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.BowelHabitsComboBox.FormattingEnabled = true;
            this.BowelHabitsComboBox.Items.AddRange(new object[] {
            "每日",
            "兩日",
            "三日",
            "四日以上"});
            this.BowelHabitsComboBox.Location = new System.Drawing.Point(104, 160);
            this.BowelHabitsComboBox.Name = "BowelHabitsComboBox";
            this.BowelHabitsComboBox.Size = new System.Drawing.Size(121, 23);
            this.BowelHabitsComboBox.TabIndex = 34;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(27, 162);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(71, 15);
            this.label11.TabIndex = 33;
            this.label11.Text = "排便習慣";
            // 
            // BetelNutCheckBox
            // 
            this.BetelNutCheckBox.AutoSize = true;
            this.BetelNutCheckBox.Location = new System.Drawing.Point(164, 134);
            this.BetelNutCheckBox.Name = "BetelNutCheckBox";
            this.BetelNutCheckBox.Size = new System.Drawing.Size(61, 19);
            this.BetelNutCheckBox.TabIndex = 32;
            this.BetelNutCheckBox.Text = "檳榔";
            this.BetelNutCheckBox.UseVisualStyleBackColor = true;
            this.BetelNutCheckBox.CheckedChanged += new System.EventHandler(this.BetelNutCheckBox_CheckedChanged);
            // 
            // DrunkCheckBox
            // 
            this.DrunkCheckBox.AutoSize = true;
            this.DrunkCheckBox.Location = new System.Drawing.Point(97, 134);
            this.DrunkCheckBox.Name = "DrunkCheckBox";
            this.DrunkCheckBox.Size = new System.Drawing.Size(61, 19);
            this.DrunkCheckBox.TabIndex = 31;
            this.DrunkCheckBox.Text = "喝酒";
            this.DrunkCheckBox.UseVisualStyleBackColor = true;
            this.DrunkCheckBox.CheckedChanged += new System.EventHandler(this.DrunkCheckBox_CheckedChanged);
            // 
            // SmokeCheckBox
            // 
            this.SmokeCheckBox.AutoSize = true;
            this.SmokeCheckBox.Location = new System.Drawing.Point(30, 134);
            this.SmokeCheckBox.Name = "SmokeCheckBox";
            this.SmokeCheckBox.Size = new System.Drawing.Size(61, 19);
            this.SmokeCheckBox.TabIndex = 30;
            this.SmokeCheckBox.Text = "吸菸";
            this.SmokeCheckBox.UseVisualStyleBackColor = true;
            this.SmokeCheckBox.CheckedChanged += new System.EventHandler(this.SmokeCheckBox_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.SportsRadioButton1);
            this.groupBox1.Controls.Add(this.SportsRadioButton2);
            this.groupBox1.Location = new System.Drawing.Point(104, 89);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(125, 37);
            this.groupBox1.TabIndex = 29;
            this.groupBox1.TabStop = false;
            // 
            // SportsRadioButton1
            // 
            this.SportsRadioButton1.AutoSize = true;
            this.SportsRadioButton1.Location = new System.Drawing.Point(3, 15);
            this.SportsRadioButton1.Name = "SportsRadioButton1";
            this.SportsRadioButton1.Size = new System.Drawing.Size(44, 19);
            this.SportsRadioButton1.TabIndex = 26;
            this.SportsRadioButton1.TabStop = true;
            this.SportsRadioButton1.Text = "有";
            this.SportsRadioButton1.UseVisualStyleBackColor = true;
            this.SportsRadioButton1.CheckedChanged += new System.EventHandler(this.SportsRadioButton1_CheckedChanged);
            // 
            // SportsRadioButton2
            // 
            this.SportsRadioButton2.AutoSize = true;
            this.SportsRadioButton2.Location = new System.Drawing.Point(81, 15);
            this.SportsRadioButton2.Name = "SportsRadioButton2";
            this.SportsRadioButton2.Size = new System.Drawing.Size(44, 19);
            this.SportsRadioButton2.TabIndex = 27;
            this.SportsRadioButton2.TabStop = true;
            this.SportsRadioButton2.Text = "無";
            this.SportsRadioButton2.UseVisualStyleBackColor = true;
            this.SportsRadioButton2.CheckedChanged += new System.EventHandler(this.SportsRadioButton2_CheckedChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(27, 101);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(71, 15);
            this.label10.TabIndex = 4;
            this.label10.Text = "運動習慣";
            // 
            // BreakfastHabitComboBox
            // 
            this.BreakfastHabitComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.BreakfastHabitComboBox.FormattingEnabled = true;
            this.BreakfastHabitComboBox.Items.AddRange(new object[] {
            "都不吃",
            "有時吃",
            "每天吃"});
            this.BreakfastHabitComboBox.Location = new System.Drawing.Point(104, 63);
            this.BreakfastHabitComboBox.Name = "BreakfastHabitComboBox";
            this.BreakfastHabitComboBox.Size = new System.Drawing.Size(122, 23);
            this.BreakfastHabitComboBox.TabIndex = 3;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(27, 66);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(71, 15);
            this.label7.TabIndex = 2;
            this.label7.Text = "早餐習慣";
            // 
            // SleepHabitsComboBox
            // 
            this.SleepHabitsComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SleepHabitsComboBox.FormattingEnabled = true;
            this.SleepHabitsComboBox.Items.AddRange(new object[] {
            "七小時",
            "不足七小時",
            "失眠"});
            this.SleepHabitsComboBox.Location = new System.Drawing.Point(105, 30);
            this.SleepHabitsComboBox.Name = "SleepHabitsComboBox";
            this.SleepHabitsComboBox.Size = new System.Drawing.Size(121, 23);
            this.SleepHabitsComboBox.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(27, 36);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 15);
            this.label5.TabIndex = 0;
            this.label5.Text = "睡眠習慣";
            // 
            // HealthInfoGroupBox
            // 
            this.HealthInfoGroupBox.BackColor = System.Drawing.Color.Transparent;
            this.HealthInfoGroupBox.Controls.Add(this.cb18Text);
            this.HealthInfoGroupBox.Controls.Add(this.cb17Text);
            this.HealthInfoGroupBox.Controls.Add(this.cb16Text);
            this.HealthInfoGroupBox.Controls.Add(this.cb14Text);
            this.HealthInfoGroupBox.Controls.Add(this.cb13Text);
            this.HealthInfoGroupBox.Controls.Add(this.checkBox18);
            this.HealthInfoGroupBox.Controls.Add(this.checkBox17);
            this.HealthInfoGroupBox.Controls.Add(this.checkBox16);
            this.HealthInfoGroupBox.Controls.Add(this.checkBox15);
            this.HealthInfoGroupBox.Controls.Add(this.checkBox14);
            this.HealthInfoGroupBox.Controls.Add(this.checkBox13);
            this.HealthInfoGroupBox.Controls.Add(this.checkBox12);
            this.HealthInfoGroupBox.Controls.Add(this.checkBox11);
            this.HealthInfoGroupBox.Controls.Add(this.checkBox10);
            this.HealthInfoGroupBox.Controls.Add(this.checkBox9);
            this.HealthInfoGroupBox.Controls.Add(this.checkBox8);
            this.HealthInfoGroupBox.Controls.Add(this.checkBox7);
            this.HealthInfoGroupBox.Controls.Add(this.checkBox6);
            this.HealthInfoGroupBox.Controls.Add(this.checkBox5);
            this.HealthInfoGroupBox.Controls.Add(this.checkBox4);
            this.HealthInfoGroupBox.Controls.Add(this.checkBox3);
            this.HealthInfoGroupBox.Controls.Add(this.checkBox2);
            this.HealthInfoGroupBox.Controls.Add(this.checkBox1);
            this.HealthInfoGroupBox.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.HealthInfoGroupBox.ForeColor = System.Drawing.SystemColors.ControlText;
            this.HealthInfoGroupBox.Location = new System.Drawing.Point(8, 193);
            this.HealthInfoGroupBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.HealthInfoGroupBox.Name = "HealthInfoGroupBox";
            this.HealthInfoGroupBox.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.HealthInfoGroupBox.Size = new System.Drawing.Size(641, 228);
            this.HealthInfoGroupBox.TabIndex = 31;
            this.HealthInfoGroupBox.TabStop = false;
            this.HealthInfoGroupBox.Text = "病例基本資料";
            // 
            // cb18Text
            // 
            this.cb18Text.Enabled = false;
            this.cb18Text.Location = new System.Drawing.Point(331, 177);
            this.cb18Text.Name = "cb18Text";
            this.cb18Text.Size = new System.Drawing.Size(243, 25);
            this.cb18Text.TabIndex = 22;
            // 
            // cb17Text
            // 
            this.cb17Text.Enabled = false;
            this.cb17Text.Location = new System.Drawing.Point(393, 148);
            this.cb17Text.Name = "cb17Text";
            this.cb17Text.Size = new System.Drawing.Size(181, 25);
            this.cb17Text.TabIndex = 21;
            // 
            // cb16Text
            // 
            this.cb16Text.Enabled = false;
            this.cb16Text.Location = new System.Drawing.Point(393, 116);
            this.cb16Text.Name = "cb16Text";
            this.cb16Text.Size = new System.Drawing.Size(181, 25);
            this.cb16Text.TabIndex = 20;
            // 
            // cb14Text
            // 
            this.cb14Text.Enabled = false;
            this.cb14Text.Location = new System.Drawing.Point(330, 63);
            this.cb14Text.Name = "cb14Text";
            this.cb14Text.Size = new System.Drawing.Size(244, 25);
            this.cb14Text.TabIndex = 19;
            // 
            // cb13Text
            // 
            this.cb13Text.Enabled = false;
            this.cb13Text.Location = new System.Drawing.Point(410, 33);
            this.cb13Text.Name = "cb13Text";
            this.cb13Text.Size = new System.Drawing.Size(164, 25);
            this.cb13Text.TabIndex = 18;
            // 
            // checkBox18
            // 
            this.checkBox18.AutoSize = true;
            this.checkBox18.Location = new System.Drawing.Point(263, 180);
            this.checkBox18.Name = "checkBox18";
            this.checkBox18.Size = new System.Drawing.Size(61, 19);
            this.checkBox18.TabIndex = 17;
            this.checkBox18.Text = "其他";
            this.checkBox18.UseVisualStyleBackColor = true;
            this.checkBox18.CheckedChanged += new System.EventHandler(this.checkBox18_CheckedChanged);
            // 
            // checkBox17
            // 
            this.checkBox17.AutoSize = true;
            this.checkBox17.Location = new System.Drawing.Point(263, 150);
            this.checkBox17.Name = "checkBox17";
            this.checkBox17.Size = new System.Drawing.Size(125, 19);
            this.checkBox17.TabIndex = 16;
            this.checkBox17.Text = "過敏物質名稱";
            this.checkBox17.UseVisualStyleBackColor = true;
            this.checkBox17.CheckedChanged += new System.EventHandler(this.checkBox17_CheckedChanged);
            // 
            // checkBox16
            // 
            this.checkBox16.AutoSize = true;
            this.checkBox16.Location = new System.Drawing.Point(263, 118);
            this.checkBox16.Name = "checkBox16";
            this.checkBox16.Size = new System.Drawing.Size(125, 19);
            this.checkBox16.TabIndex = 15;
            this.checkBox16.Text = "重大手術名稱";
            this.checkBox16.UseVisualStyleBackColor = true;
            this.checkBox16.CheckedChanged += new System.EventHandler(this.checkBox16_CheckedChanged);
            // 
            // checkBox15
            // 
            this.checkBox15.AutoSize = true;
            this.checkBox15.Location = new System.Drawing.Point(263, 91);
            this.checkBox15.Name = "checkBox15";
            this.checkBox15.Size = new System.Drawing.Size(109, 19);
            this.checkBox15.TabIndex = 14;
            this.checkBox15.Text = "海洋性貧血";
            this.checkBox15.UseVisualStyleBackColor = true;
            this.checkBox15.CheckedChanged += new System.EventHandler(this.checkBox15_CheckedChanged);
            // 
            // checkBox14
            // 
            this.checkBox14.AutoSize = true;
            this.checkBox14.Location = new System.Drawing.Point(263, 65);
            this.checkBox14.Name = "checkBox14";
            this.checkBox14.Size = new System.Drawing.Size(61, 19);
            this.checkBox14.TabIndex = 13;
            this.checkBox14.Text = "癌症";
            this.checkBox14.UseVisualStyleBackColor = true;
            this.checkBox14.CheckedChanged += new System.EventHandler(this.checkBox14_CheckedChanged);
            // 
            // checkBox13
            // 
            this.checkBox13.AutoSize = true;
            this.checkBox13.Location = new System.Drawing.Point(263, 35);
            this.checkBox13.Name = "checkBox13";
            this.checkBox13.Size = new System.Drawing.Size(141, 19);
            this.checkBox13.TabIndex = 12;
            this.checkBox13.Text = "心理或精神疾病";
            this.checkBox13.UseVisualStyleBackColor = true;
            this.checkBox13.CheckedChanged += new System.EventHandler(this.checkBox13_CheckedChanged);
            // 
            // checkBox12
            // 
            this.checkBox12.AutoSize = true;
            this.checkBox12.Location = new System.Drawing.Point(141, 180);
            this.checkBox12.Name = "checkBox12";
            this.checkBox12.Size = new System.Drawing.Size(77, 19);
            this.checkBox12.TabIndex = 11;
            this.checkBox12.Text = "糖尿病";
            this.checkBox12.UseVisualStyleBackColor = true;
            this.checkBox12.CheckedChanged += new System.EventHandler(this.checkBox12_CheckedChanged);
            // 
            // checkBox11
            // 
            this.checkBox11.AutoSize = true;
            this.checkBox11.Location = new System.Drawing.Point(141, 150);
            this.checkBox11.Name = "checkBox11";
            this.checkBox11.Size = new System.Drawing.Size(77, 19);
            this.checkBox11.TabIndex = 10;
            this.checkBox11.Text = "關節炎";
            this.checkBox11.UseVisualStyleBackColor = true;
            this.checkBox11.CheckedChanged += new System.EventHandler(this.checkBox11_CheckedChanged);
            // 
            // checkBox10
            // 
            this.checkBox10.AutoSize = true;
            this.checkBox10.Location = new System.Drawing.Point(141, 118);
            this.checkBox10.Name = "checkBox10";
            this.checkBox10.Size = new System.Drawing.Size(77, 19);
            this.checkBox10.TabIndex = 9;
            this.checkBox10.Text = "蠶豆症";
            this.checkBox10.UseVisualStyleBackColor = true;
            this.checkBox10.CheckedChanged += new System.EventHandler(this.checkBox10_CheckedChanged);
            // 
            // checkBox9
            // 
            this.checkBox9.AutoSize = true;
            this.checkBox9.Location = new System.Drawing.Point(141, 91);
            this.checkBox9.Name = "checkBox9";
            this.checkBox9.Size = new System.Drawing.Size(77, 19);
            this.checkBox9.TabIndex = 8;
            this.checkBox9.Text = "血友病";
            this.checkBox9.UseVisualStyleBackColor = true;
            this.checkBox9.CheckedChanged += new System.EventHandler(this.checkBox9_CheckedChanged);
            // 
            // checkBox8
            // 
            this.checkBox8.AutoSize = true;
            this.checkBox8.Location = new System.Drawing.Point(141, 65);
            this.checkBox8.Name = "checkBox8";
            this.checkBox8.Size = new System.Drawing.Size(109, 19);
            this.checkBox8.TabIndex = 7;
            this.checkBox8.Text = "紅斑性狼瘡";
            this.checkBox8.UseVisualStyleBackColor = true;
            this.checkBox8.CheckedChanged += new System.EventHandler(this.checkBox8_CheckedChanged);
            // 
            // checkBox7
            // 
            this.checkBox7.AutoSize = true;
            this.checkBox7.Location = new System.Drawing.Point(141, 35);
            this.checkBox7.Name = "checkBox7";
            this.checkBox7.Size = new System.Drawing.Size(61, 19);
            this.checkBox7.TabIndex = 6;
            this.checkBox7.Text = "癲癇";
            this.checkBox7.UseVisualStyleBackColor = true;
            this.checkBox7.CheckedChanged += new System.EventHandler(this.checkBox7_CheckedChanged);
            // 
            // checkBox6
            // 
            this.checkBox6.AutoSize = true;
            this.checkBox6.Location = new System.Drawing.Point(26, 180);
            this.checkBox6.Name = "checkBox6";
            this.checkBox6.Size = new System.Drawing.Size(77, 19);
            this.checkBox6.TabIndex = 5;
            this.checkBox6.Text = "腎臟病";
            this.checkBox6.UseVisualStyleBackColor = true;
            this.checkBox6.CheckedChanged += new System.EventHandler(this.checkBox6_CheckedChanged);
            // 
            // checkBox5
            // 
            this.checkBox5.AutoSize = true;
            this.checkBox5.Location = new System.Drawing.Point(26, 150);
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.Size = new System.Drawing.Size(61, 19);
            this.checkBox5.TabIndex = 4;
            this.checkBox5.Text = "氣喘";
            this.checkBox5.UseVisualStyleBackColor = true;
            this.checkBox5.CheckedChanged += new System.EventHandler(this.checkBox5_CheckedChanged);
            // 
            // checkBox4
            // 
            this.checkBox4.AutoSize = true;
            this.checkBox4.Location = new System.Drawing.Point(26, 118);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(61, 19);
            this.checkBox4.TabIndex = 3;
            this.checkBox4.Text = "肝炎";
            this.checkBox4.UseVisualStyleBackColor = true;
            this.checkBox4.CheckedChanged += new System.EventHandler(this.checkBox4_CheckedChanged);
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Location = new System.Drawing.Point(26, 91);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(77, 19);
            this.checkBox3.TabIndex = 2;
            this.checkBox3.Text = "心臟病";
            this.checkBox3.UseVisualStyleBackColor = true;
            this.checkBox3.CheckedChanged += new System.EventHandler(this.checkBox3_CheckedChanged);
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(26, 65);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(77, 19);
            this.checkBox2.TabIndex = 1;
            this.checkBox2.Text = "肺結核";
            this.checkBox2.UseVisualStyleBackColor = true;
            this.checkBox2.CheckedChanged += new System.EventHandler(this.checkBox2_CheckedChanged);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(26, 35);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(45, 19);
            this.checkBox1.TabIndex = 0;
            this.checkBox1.Text = "無";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // PersonalInfoGroupBox
            // 
            this.PersonalInfoGroupBox.BackColor = System.Drawing.Color.Transparent;
            this.PersonalInfoGroupBox.Controls.Add(this.BloodTypeComboBox);
            this.PersonalInfoGroupBox.Controls.Add(this.label4);
            this.PersonalInfoGroupBox.Controls.Add(this.PatientAddress);
            this.PersonalInfoGroupBox.Controls.Add(this.label3);
            this.PersonalInfoGroupBox.Controls.Add(this.PatientPhone);
            this.PersonalInfoGroupBox.Controls.Add(this.label2);
            this.PersonalInfoGroupBox.Controls.Add(this.genderGroupBox);
            this.PersonalInfoGroupBox.Controls.Add(this.label1);
            this.PersonalInfoGroupBox.Controls.Add(this.label6);
            this.PersonalInfoGroupBox.Controls.Add(this.PatientBirthday);
            this.PersonalInfoGroupBox.Controls.Add(this.label8);
            this.PersonalInfoGroupBox.Controls.Add(this.PatientName);
            this.PersonalInfoGroupBox.Controls.Add(this.PatientID);
            this.PersonalInfoGroupBox.Controls.Add(this.label9);
            this.PersonalInfoGroupBox.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.PersonalInfoGroupBox.ForeColor = System.Drawing.SystemColors.ControlText;
            this.PersonalInfoGroupBox.Location = new System.Drawing.Point(6, 44);
            this.PersonalInfoGroupBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.PersonalInfoGroupBox.Name = "PersonalInfoGroupBox";
            this.PersonalInfoGroupBox.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.PersonalInfoGroupBox.Size = new System.Drawing.Size(931, 133);
            this.PersonalInfoGroupBox.TabIndex = 25;
            this.PersonalInfoGroupBox.TabStop = false;
            this.PersonalInfoGroupBox.Text = "病例基本資料";
            // 
            // BloodTypeComboBox
            // 
            this.BloodTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.BloodTypeComboBox.FormattingEnabled = true;
            this.BloodTypeComboBox.Items.AddRange(new object[] {
            "A",
            "B",
            "AB",
            "O"});
            this.BloodTypeComboBox.Location = new System.Drawing.Point(725, 48);
            this.BloodTypeComboBox.Name = "BloodTypeComboBox";
            this.BloodTypeComboBox.Size = new System.Drawing.Size(121, 23);
            this.BloodTypeComboBox.TabIndex = 34;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(679, 48);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(39, 15);
            this.label4.TabIndex = 33;
            this.label4.Text = "血型";
            // 
            // PatientAddress
            // 
            this.PatientAddress.Location = new System.Drawing.Point(540, 88);
            this.PatientAddress.MaxLength = 100;
            this.PatientAddress.Name = "PatientAddress";
            this.PatientAddress.ShortcutsEnabled = false;
            this.PatientAddress.Size = new System.Drawing.Size(306, 25);
            this.PatientAddress.TabIndex = 32;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(474, 91);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 15);
            this.label3.TabIndex = 31;
            this.label3.Text = "地址";
            // 
            // PatientPhone
            // 
            this.PatientPhone.Location = new System.Drawing.Point(540, 45);
            this.PatientPhone.MaxLength = 10;
            this.PatientPhone.Name = "PatientPhone";
            this.PatientPhone.ShortcutsEnabled = false;
            this.PatientPhone.Size = new System.Drawing.Size(125, 25);
            this.PatientPhone.TabIndex = 30;
            this.PatientPhone.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.PatientPhone_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(474, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 15);
            this.label2.TabIndex = 29;
            this.label2.Text = "電話";
            // 
            // genderGroupBox
            // 
            this.genderGroupBox.Controls.Add(this.maleRB);
            this.genderGroupBox.Controls.Add(this.femaleRB);
            this.genderGroupBox.Location = new System.Drawing.Point(343, 75);
            this.genderGroupBox.Name = "genderGroupBox";
            this.genderGroupBox.Size = new System.Drawing.Size(125, 45);
            this.genderGroupBox.TabIndex = 28;
            this.genderGroupBox.TabStop = false;
            // 
            // maleRB
            // 
            this.maleRB.AutoSize = true;
            this.maleRB.Location = new System.Drawing.Point(3, 17);
            this.maleRB.Name = "maleRB";
            this.maleRB.Size = new System.Drawing.Size(44, 19);
            this.maleRB.TabIndex = 26;
            this.maleRB.TabStop = true;
            this.maleRB.Text = "男";
            this.maleRB.UseVisualStyleBackColor = true;
            this.maleRB.CheckedChanged += new System.EventHandler(this.maleRB_CheckedChanged);
            // 
            // femaleRB
            // 
            this.femaleRB.AutoSize = true;
            this.femaleRB.Location = new System.Drawing.Point(81, 17);
            this.femaleRB.Name = "femaleRB";
            this.femaleRB.Size = new System.Drawing.Size(44, 19);
            this.femaleRB.TabIndex = 27;
            this.femaleRB.TabStop = true;
            this.femaleRB.Text = "女";
            this.femaleRB.UseVisualStyleBackColor = true;
            this.femaleRB.CheckedChanged += new System.EventHandler(this.femaleRB_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(231, 95);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 15);
            this.label1.TabIndex = 25;
            this.label1.Text = "性別";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 95);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(39, 15);
            this.label6.TabIndex = 24;
            this.label6.Text = "生日";
            // 
            // PatientBirthday
            // 
            this.PatientBirthday.CustomFormat = "yyyy/MM/dd";
            this.PatientBirthday.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.PatientBirthday.Location = new System.Drawing.Point(100, 88);
            this.PatientBirthday.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.PatientBirthday.Name = "PatientBirthday";
            this.PatientBirthday.Size = new System.Drawing.Size(125, 25);
            this.PatientBirthday.TabIndex = 18;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 48);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(39, 15);
            this.label8.TabIndex = 14;
            this.label8.Text = "姓名";
            // 
            // PatientName
            // 
            this.PatientName.Location = new System.Drawing.Point(100, 45);
            this.PatientName.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.PatientName.MaxLength = 20;
            this.PatientName.Name = "PatientName";
            this.PatientName.Size = new System.Drawing.Size(125, 25);
            this.PatientName.TabIndex = 19;
            // 
            // PatientID
            // 
            this.PatientID.Cursor = System.Windows.Forms.Cursors.SizeAll;
            this.PatientID.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.PatientID.Location = new System.Drawing.Point(343, 45);
            this.PatientID.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.PatientID.MaxLength = 10;
            this.PatientID.Name = "PatientID";
            this.PatientID.ShortcutsEnabled = false;
            this.PatientID.Size = new System.Drawing.Size(125, 25);
            this.PatientID.TabIndex = 20;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(231, 48);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(87, 15);
            this.label9.TabIndex = 15;
            this.label9.Text = "身分證字號";
            // 
            // CallLoginBtn
            // 
            this.CallLoginBtn.Location = new System.Drawing.Point(842, 5);
            this.CallLoginBtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.CallLoginBtn.Name = "CallLoginBtn";
            this.CallLoginBtn.Size = new System.Drawing.Size(96, 34);
            this.CallLoginBtn.TabIndex = 12;
            this.CallLoginBtn.Text = "登入";
            this.CallLoginBtn.UseVisualStyleBackColor = true;
            this.CallLoginBtn.Click += new System.EventHandler(this.CallLoginBtn_Click);
            // 
            // AdviserName
            // 
            this.AdviserName.AutoSize = true;
            this.AdviserName.Location = new System.Drawing.Point(3, 3);
            this.AdviserName.Name = "AdviserName";
            this.AdviserName.Size = new System.Drawing.Size(52, 15);
            this.AdviserName.TabIndex = 0;
            this.AdviserName.Text = "未登入";
            // 
            // InsertTreatment
            // 
            this.InsertTreatment.Location = new System.Drawing.Point(803, 465);
            this.InsertTreatment.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.InsertTreatment.Name = "InsertTreatment";
            this.InsertTreatment.Size = new System.Drawing.Size(96, 34);
            this.InsertTreatment.TabIndex = 23;
            this.InsertTreatment.Text = "匯入資料";
            this.InsertTreatment.UseVisualStyleBackColor = true;
            this.InsertTreatment.Click += new System.EventHandler(this.InsertTreatment_Click);
            // 
            // singleSelect
            // 
            this.singleSelect.BackColor = System.Drawing.Color.WhiteSmoke;
            this.singleSelect.Controls.Add(this.ExportWord);
            this.singleSelect.Controls.Add(this.preview);
            this.singleSelect.Controls.Add(this.patientlistbox);
            this.singleSelect.Controls.Add(this.label15);
            this.singleSelect.Location = new System.Drawing.Point(4, 25);
            this.singleSelect.Name = "singleSelect";
            this.singleSelect.Padding = new System.Windows.Forms.Padding(3);
            this.singleSelect.Size = new System.Drawing.Size(944, 536);
            this.singleSelect.TabIndex = 1;
            this.singleSelect.Text = "個人查詢";
            // 
            // ExportWord
            // 
            this.ExportWord.Location = new System.Drawing.Point(686, 468);
            this.ExportWord.Name = "ExportWord";
            this.ExportWord.Size = new System.Drawing.Size(229, 44);
            this.ExportWord.TabIndex = 5;
            this.ExportWord.Text = "Export";
            this.ExportWord.UseVisualStyleBackColor = true;
            this.ExportWord.Click += new System.EventHandler(this.ExportWord_Click);
            // 
            // preview
            // 
            this.preview.Controls.Add(this.RVAdvisorName);
            this.preview.Controls.Add(this.RVSymptom);
            this.preview.Controls.Add(this.RVWorking);
            this.preview.Controls.Add(this.RVSmokeDrunkBetelNut);
            this.preview.Controls.Add(this.RVHealthInfoResult);
            this.preview.Controls.Add(this.RVPatientBirthday);
            this.preview.Controls.Add(this.RVSmokeDrunkBetelNutlb);
            this.preview.Controls.Add(this.RVBowel);
            this.preview.Controls.Add(this.RVSport);
            this.preview.Controls.Add(this.RVBreakfast);
            this.preview.Controls.Add(this.RVSleep);
            this.preview.Controls.Add(this.RVPatientAddress);
            this.preview.Controls.Add(this.RVPatientPhone);
            this.preview.Controls.Add(this.RVPatientGender);
            this.preview.Controls.Add(this.RVBloodType);
            this.preview.Controls.Add(this.RVPatientID);
            this.preview.Controls.Add(this.RVPatientName);
            this.preview.Controls.Add(this.RVfirstvisitdate);
            this.preview.Controls.Add(this.RVSymptomlb);
            this.preview.Controls.Add(this.RVAdvisorNamelb);
            this.preview.Controls.Add(this.RVWorkinglb);
            this.preview.Controls.Add(this.RVBowellb);
            this.preview.Controls.Add(this.RVSportlb);
            this.preview.Controls.Add(this.RVBreakfastlb);
            this.preview.Controls.Add(this.RVSleeplb);
            this.preview.Controls.Add(this.RVHealthInfoResultlb);
            this.preview.Controls.Add(this.RVPatientAddresslb);
            this.preview.Controls.Add(this.RVBloodTypelb);
            this.preview.Controls.Add(this.RVPatientGenderlb);
            this.preview.Controls.Add(this.RVPatientBirthdaylb);
            this.preview.Controls.Add(this.RVPatientPhonelb);
            this.preview.Controls.Add(this.RVPatientIDlb);
            this.preview.Controls.Add(this.RVPatientNamelb);
            this.preview.Controls.Add(this.RVfirstvisitdatelb);
            this.preview.Location = new System.Drawing.Point(228, 13);
            this.preview.Name = "preview";
            this.preview.Size = new System.Drawing.Size(441, 499);
            this.preview.TabIndex = 4;
            this.preview.TabStop = false;
            this.preview.Text = "預覽";
            // 
            // RVAdvisorName
            // 
            this.RVAdvisorName.AutoSize = true;
            this.RVAdvisorName.Location = new System.Drawing.Point(279, 475);
            this.RVAdvisorName.Name = "RVAdvisorName";
            this.RVAdvisorName.Size = new System.Drawing.Size(0, 15);
            this.RVAdvisorName.TabIndex = 35;
            // 
            // RVSymptom
            // 
            this.RVSymptom.Location = new System.Drawing.Point(50, 368);
            this.RVSymptom.Name = "RVSymptom";
            this.RVSymptom.Size = new System.Drawing.Size(402, 100);
            this.RVSymptom.TabIndex = 34;
            // 
            // RVWorking
            // 
            this.RVWorking.AutoSize = true;
            this.RVWorking.Location = new System.Drawing.Point(80, 338);
            this.RVWorking.Name = "RVWorking";
            this.RVWorking.Size = new System.Drawing.Size(0, 15);
            this.RVWorking.TabIndex = 33;
            // 
            // RVSmokeDrunkBetelNut
            // 
            this.RVSmokeDrunkBetelNut.AutoSize = true;
            this.RVSmokeDrunkBetelNut.Location = new System.Drawing.Point(110, 311);
            this.RVSmokeDrunkBetelNut.Name = "RVSmokeDrunkBetelNut";
            this.RVSmokeDrunkBetelNut.Size = new System.Drawing.Size(0, 15);
            this.RVSmokeDrunkBetelNut.TabIndex = 32;
            // 
            // RVHealthInfoResult
            // 
            this.RVHealthInfoResult.Location = new System.Drawing.Point(49, 165);
            this.RVHealthInfoResult.Name = "RVHealthInfoResult";
            this.RVHealthInfoResult.Size = new System.Drawing.Size(386, 70);
            this.RVHealthInfoResult.TabIndex = 25;
            // 
            // RVPatientBirthday
            // 
            this.RVPatientBirthday.AutoSize = true;
            this.RVPatientBirthday.Location = new System.Drawing.Point(49, 96);
            this.RVPatientBirthday.Name = "RVPatientBirthday";
            this.RVPatientBirthday.Size = new System.Drawing.Size(0, 15);
            this.RVPatientBirthday.TabIndex = 31;
            // 
            // RVSmokeDrunkBetelNutlb
            // 
            this.RVSmokeDrunkBetelNutlb.AutoSize = true;
            this.RVSmokeDrunkBetelNutlb.Location = new System.Drawing.Point(6, 311);
            this.RVSmokeDrunkBetelNutlb.Name = "RVSmokeDrunkBetelNutlb";
            this.RVSmokeDrunkBetelNutlb.Size = new System.Drawing.Size(97, 15);
            this.RVSmokeDrunkBetelNutlb.TabIndex = 30;
            this.RVSmokeDrunkBetelNutlb.Text = "菸酒檳榔習慣";
            // 
            // RVBowel
            // 
            this.RVBowel.AutoSize = true;
            this.RVBowel.Location = new System.Drawing.Point(282, 280);
            this.RVBowel.Name = "RVBowel";
            this.RVBowel.Size = new System.Drawing.Size(0, 15);
            this.RVBowel.TabIndex = 29;
            // 
            // RVSport
            // 
            this.RVSport.AutoSize = true;
            this.RVSport.Location = new System.Drawing.Point(79, 280);
            this.RVSport.Name = "RVSport";
            this.RVSport.Size = new System.Drawing.Size(0, 15);
            this.RVSport.TabIndex = 28;
            // 
            // RVBreakfast
            // 
            this.RVBreakfast.AutoSize = true;
            this.RVBreakfast.Location = new System.Drawing.Point(279, 246);
            this.RVBreakfast.Name = "RVBreakfast";
            this.RVBreakfast.Size = new System.Drawing.Size(0, 15);
            this.RVBreakfast.TabIndex = 27;
            // 
            // RVSleep
            // 
            this.RVSleep.AutoSize = true;
            this.RVSleep.Location = new System.Drawing.Point(79, 246);
            this.RVSleep.Name = "RVSleep";
            this.RVSleep.Size = new System.Drawing.Size(0, 15);
            this.RVSleep.TabIndex = 26;
            // 
            // RVPatientAddress
            // 
            this.RVPatientAddress.AutoSize = true;
            this.RVPatientAddress.Location = new System.Drawing.Point(49, 130);
            this.RVPatientAddress.Name = "RVPatientAddress";
            this.RVPatientAddress.Size = new System.Drawing.Size(0, 15);
            this.RVPatientAddress.TabIndex = 24;
            // 
            // RVPatientPhone
            // 
            this.RVPatientPhone.AutoSize = true;
            this.RVPatientPhone.Location = new System.Drawing.Point(278, 96);
            this.RVPatientPhone.Name = "RVPatientPhone";
            this.RVPatientPhone.Size = new System.Drawing.Size(0, 15);
            this.RVPatientPhone.TabIndex = 23;
            // 
            // RVPatientGender
            // 
            this.RVPatientGender.AutoSize = true;
            this.RVPatientGender.Location = new System.Drawing.Point(178, 96);
            this.RVPatientGender.Name = "RVPatientGender";
            this.RVPatientGender.Size = new System.Drawing.Size(0, 15);
            this.RVPatientGender.TabIndex = 22;
            // 
            // RVBloodType
            // 
            this.RVBloodType.AutoSize = true;
            this.RVBloodType.Location = new System.Drawing.Point(353, 60);
            this.RVBloodType.Name = "RVBloodType";
            this.RVBloodType.Size = new System.Drawing.Size(0, 15);
            this.RVBloodType.TabIndex = 21;
            // 
            // RVPatientID
            // 
            this.RVPatientID.AutoSize = true;
            this.RVPatientID.Location = new System.Drawing.Point(193, 60);
            this.RVPatientID.Name = "RVPatientID";
            this.RVPatientID.Size = new System.Drawing.Size(0, 15);
            this.RVPatientID.TabIndex = 20;
            // 
            // RVPatientName
            // 
            this.RVPatientName.AutoSize = true;
            this.RVPatientName.Location = new System.Drawing.Point(49, 60);
            this.RVPatientName.Name = "RVPatientName";
            this.RVPatientName.Size = new System.Drawing.Size(0, 15);
            this.RVPatientName.TabIndex = 19;
            // 
            // RVfirstvisitdate
            // 
            this.RVfirstvisitdate.AutoSize = true;
            this.RVfirstvisitdate.Location = new System.Drawing.Point(108, 30);
            this.RVfirstvisitdate.Name = "RVfirstvisitdate";
            this.RVfirstvisitdate.Size = new System.Drawing.Size(0, 15);
            this.RVfirstvisitdate.TabIndex = 18;
            // 
            // RVSymptomlb
            // 
            this.RVSymptomlb.AutoSize = true;
            this.RVSymptomlb.Location = new System.Drawing.Point(6, 368);
            this.RVSymptomlb.Name = "RVSymptomlb";
            this.RVSymptomlb.Size = new System.Drawing.Size(37, 15);
            this.RVSymptomlb.TabIndex = 17;
            this.RVSymptomlb.Text = "症狀";
            // 
            // RVAdvisorNamelb
            // 
            this.RVAdvisorNamelb.AutoSize = true;
            this.RVAdvisorNamelb.Location = new System.Drawing.Point(236, 475);
            this.RVAdvisorNamelb.Name = "RVAdvisorNamelb";
            this.RVAdvisorNamelb.Size = new System.Drawing.Size(37, 15);
            this.RVAdvisorNamelb.TabIndex = 15;
            this.RVAdvisorNamelb.Text = "醫生";
            // 
            // RVWorkinglb
            // 
            this.RVWorkinglb.AutoSize = true;
            this.RVWorkinglb.Location = new System.Drawing.Point(6, 338);
            this.RVWorkinglb.Name = "RVWorkinglb";
            this.RVWorkinglb.Size = new System.Drawing.Size(67, 15);
            this.RVWorkinglb.TabIndex = 14;
            this.RVWorkinglb.Text = "工作時間";
            // 
            // RVBowellb
            // 
            this.RVBowellb.AutoSize = true;
            this.RVBowellb.Location = new System.Drawing.Point(205, 280);
            this.RVBowellb.Name = "RVBowellb";
            this.RVBowellb.Size = new System.Drawing.Size(67, 15);
            this.RVBowellb.TabIndex = 13;
            this.RVBowellb.Text = "排便習慣";
            // 
            // RVSportlb
            // 
            this.RVSportlb.AutoSize = true;
            this.RVSportlb.Location = new System.Drawing.Point(6, 280);
            this.RVSportlb.Name = "RVSportlb";
            this.RVSportlb.Size = new System.Drawing.Size(67, 15);
            this.RVSportlb.TabIndex = 11;
            this.RVSportlb.Text = "運動習慣";
            // 
            // RVBreakfastlb
            // 
            this.RVBreakfastlb.AutoSize = true;
            this.RVBreakfastlb.Location = new System.Drawing.Point(205, 246);
            this.RVBreakfastlb.Name = "RVBreakfastlb";
            this.RVBreakfastlb.Size = new System.Drawing.Size(67, 15);
            this.RVBreakfastlb.TabIndex = 10;
            this.RVBreakfastlb.Text = "早餐習慣";
            // 
            // RVSleeplb
            // 
            this.RVSleeplb.AutoSize = true;
            this.RVSleeplb.Location = new System.Drawing.Point(6, 246);
            this.RVSleeplb.Name = "RVSleeplb";
            this.RVSleeplb.Size = new System.Drawing.Size(67, 15);
            this.RVSleeplb.TabIndex = 9;
            this.RVSleeplb.Text = "睡眠習慣";
            // 
            // RVHealthInfoResultlb
            // 
            this.RVHealthInfoResultlb.AutoSize = true;
            this.RVHealthInfoResultlb.Location = new System.Drawing.Point(6, 165);
            this.RVHealthInfoResultlb.Name = "RVHealthInfoResultlb";
            this.RVHealthInfoResultlb.Size = new System.Drawing.Size(37, 15);
            this.RVHealthInfoResultlb.TabIndex = 8;
            this.RVHealthInfoResultlb.Text = "病例";
            // 
            // RVPatientAddresslb
            // 
            this.RVPatientAddresslb.AutoSize = true;
            this.RVPatientAddresslb.Location = new System.Drawing.Point(6, 130);
            this.RVPatientAddresslb.Name = "RVPatientAddresslb";
            this.RVPatientAddresslb.Size = new System.Drawing.Size(37, 15);
            this.RVPatientAddresslb.TabIndex = 7;
            this.RVPatientAddresslb.Text = "地址";
            // 
            // RVBloodTypelb
            // 
            this.RVBloodTypelb.AutoSize = true;
            this.RVBloodTypelb.Location = new System.Drawing.Point(310, 60);
            this.RVBloodTypelb.Name = "RVBloodTypelb";
            this.RVBloodTypelb.Size = new System.Drawing.Size(37, 15);
            this.RVBloodTypelb.TabIndex = 6;
            this.RVBloodTypelb.Text = "血型";
            // 
            // RVPatientGenderlb
            // 
            this.RVPatientGenderlb.AutoSize = true;
            this.RVPatientGenderlb.Location = new System.Drawing.Point(135, 96);
            this.RVPatientGenderlb.Name = "RVPatientGenderlb";
            this.RVPatientGenderlb.Size = new System.Drawing.Size(37, 15);
            this.RVPatientGenderlb.TabIndex = 5;
            this.RVPatientGenderlb.Text = "性別";
            // 
            // RVPatientBirthdaylb
            // 
            this.RVPatientBirthdaylb.AutoSize = true;
            this.RVPatientBirthdaylb.Location = new System.Drawing.Point(6, 96);
            this.RVPatientBirthdaylb.Name = "RVPatientBirthdaylb";
            this.RVPatientBirthdaylb.Size = new System.Drawing.Size(37, 15);
            this.RVPatientBirthdaylb.TabIndex = 4;
            this.RVPatientBirthdaylb.Text = "生日";
            // 
            // RVPatientPhonelb
            // 
            this.RVPatientPhonelb.AutoSize = true;
            this.RVPatientPhonelb.Location = new System.Drawing.Point(235, 96);
            this.RVPatientPhonelb.Name = "RVPatientPhonelb";
            this.RVPatientPhonelb.Size = new System.Drawing.Size(37, 15);
            this.RVPatientPhonelb.TabIndex = 3;
            this.RVPatientPhonelb.Text = "電話";
            // 
            // RVPatientIDlb
            // 
            this.RVPatientIDlb.AutoSize = true;
            this.RVPatientIDlb.Location = new System.Drawing.Point(135, 60);
            this.RVPatientIDlb.Name = "RVPatientIDlb";
            this.RVPatientIDlb.Size = new System.Drawing.Size(52, 15);
            this.RVPatientIDlb.TabIndex = 2;
            this.RVPatientIDlb.Text = "身分證";
            // 
            // RVPatientNamelb
            // 
            this.RVPatientNamelb.AutoSize = true;
            this.RVPatientNamelb.Location = new System.Drawing.Point(6, 60);
            this.RVPatientNamelb.Name = "RVPatientNamelb";
            this.RVPatientNamelb.Size = new System.Drawing.Size(37, 15);
            this.RVPatientNamelb.TabIndex = 1;
            this.RVPatientNamelb.Text = "姓名";
            // 
            // RVfirstvisitdatelb
            // 
            this.RVfirstvisitdatelb.AutoSize = true;
            this.RVfirstvisitdatelb.Location = new System.Drawing.Point(6, 30);
            this.RVfirstvisitdatelb.Name = "RVfirstvisitdatelb";
            this.RVfirstvisitdatelb.Size = new System.Drawing.Size(67, 15);
            this.RVfirstvisitdatelb.TabIndex = 0;
            this.RVfirstvisitdatelb.Text = "初診日期";
            // 
            // patientlistbox
            // 
            this.patientlistbox.FormattingEnabled = true;
            this.patientlistbox.ItemHeight = 15;
            this.patientlistbox.Location = new System.Drawing.Point(12, 43);
            this.patientlistbox.Name = "patientlistbox";
            this.patientlistbox.Size = new System.Drawing.Size(194, 469);
            this.patientlistbox.TabIndex = 3;
            this.patientlistbox.SelectedIndexChanged += new System.EventHandler(this.patientlistbox_SelectedIndexChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(9, 13);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(67, 15);
            this.label15.TabIndex = 2;
            this.label15.Text = "病人名單";
            // 
            // HealthTool
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(953, 566);
            this.Controls.Add(this.tabControl1);
            this.Name = "HealthTool";
            this.Text = "HealthTool";
            this.Load += new System.EventHandler(this.HealthTool_Load);
            this.Resize += new System.EventHandler(this.HealthTool_Resize);
            this.tabControl1.ResumeLayout(false);
            this.dataInsert.ResumeLayout(false);
            this.dataInsert.PerformLayout();
            this.LifestyleGroupBox.ResumeLayout(false);
            this.LifestyleGroupBox.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.HealthInfoGroupBox.ResumeLayout(false);
            this.HealthInfoGroupBox.PerformLayout();
            this.PersonalInfoGroupBox.ResumeLayout(false);
            this.PersonalInfoGroupBox.PerformLayout();
            this.genderGroupBox.ResumeLayout(false);
            this.genderGroupBox.PerformLayout();
            this.singleSelect.ResumeLayout(false);
            this.singleSelect.PerformLayout();
            this.preview.ResumeLayout(false);
            this.preview.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage dataInsert;
        private System.Windows.Forms.TabPage singleSelect;
        private System.Windows.Forms.Label AdviserName;
        private System.Windows.Forms.Button CallLoginBtn;
        private System.Windows.Forms.GroupBox PersonalInfoGroupBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button InsertTreatment;
        private System.Windows.Forms.DateTimePicker PatientBirthday;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox PatientName;
        private System.Windows.Forms.TextBox PatientID;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.RadioButton femaleRB;
        private System.Windows.Forms.RadioButton maleRB;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox genderGroupBox;
        private System.Windows.Forms.TextBox PatientPhone;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox HealthInfoGroupBox;
        private System.Windows.Forms.CheckBox checkBox18;
        private System.Windows.Forms.CheckBox checkBox17;
        private System.Windows.Forms.CheckBox checkBox16;
        private System.Windows.Forms.CheckBox checkBox15;
        private System.Windows.Forms.CheckBox checkBox14;
        private System.Windows.Forms.CheckBox checkBox13;
        private System.Windows.Forms.CheckBox checkBox12;
        private System.Windows.Forms.CheckBox checkBox11;
        private System.Windows.Forms.CheckBox checkBox10;
        private System.Windows.Forms.CheckBox checkBox9;
        private System.Windows.Forms.CheckBox checkBox8;
        private System.Windows.Forms.CheckBox checkBox7;
        private System.Windows.Forms.CheckBox checkBox6;
        private System.Windows.Forms.CheckBox checkBox5;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.TextBox cb18Text;
        private System.Windows.Forms.TextBox cb17Text;
        private System.Windows.Forms.TextBox cb16Text;
        private System.Windows.Forms.TextBox cb14Text;
        private System.Windows.Forms.TextBox cb13Text;
        private System.Windows.Forms.TextBox PatientAddress;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox BloodTypeComboBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox LifestyleGroupBox;
        private System.Windows.Forms.ComboBox BreakfastHabitComboBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox BetelNutCheckBox;
        private System.Windows.Forms.CheckBox DrunkCheckBox;
        private System.Windows.Forms.CheckBox SmokeCheckBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton SportsRadioButton1;
        private System.Windows.Forms.RadioButton SportsRadioButton2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox WorkingComboBox;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox BowelHabitsComboBox;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox SymptomText;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox SleepHabitsComboBox;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.DateTimePicker FirstVisitDateTimePicker;
        private System.Windows.Forms.ListBox patientlistbox;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.GroupBox preview;
        private System.Windows.Forms.Label RVPatientGenderlb;
        private System.Windows.Forms.Label RVPatientBirthdaylb;
        private System.Windows.Forms.Label RVPatientPhonelb;
        private System.Windows.Forms.Label RVPatientIDlb;
        private System.Windows.Forms.Label RVPatientNamelb;
        private System.Windows.Forms.Label RVfirstvisitdatelb;
        private System.Windows.Forms.Label RVWorkinglb;
        private System.Windows.Forms.Label RVBowellb;
        private System.Windows.Forms.Label RVSportlb;
        private System.Windows.Forms.Label RVBreakfastlb;
        private System.Windows.Forms.Label RVSleeplb;
        private System.Windows.Forms.Label RVHealthInfoResultlb;
        private System.Windows.Forms.Label RVPatientAddresslb;
        private System.Windows.Forms.Label RVBloodTypelb;
        private System.Windows.Forms.Label RVAdvisorNamelb;
        private System.Windows.Forms.Label RVPatientName;
        private System.Windows.Forms.Label RVfirstvisitdate;
        private System.Windows.Forms.Label RVSymptomlb;
        private System.Windows.Forms.Label RVSmokeDrunkBetelNutlb;
        private System.Windows.Forms.Label RVBowel;
        private System.Windows.Forms.Label RVSport;
        private System.Windows.Forms.Label RVBreakfast;
        private System.Windows.Forms.Label RVSleep;
        private System.Windows.Forms.Label RVHealthInfoResult;
        private System.Windows.Forms.Label RVPatientAddress;
        private System.Windows.Forms.Label RVPatientPhone;
        private System.Windows.Forms.Label RVPatientGender;
        private System.Windows.Forms.Label RVBloodType;
        private System.Windows.Forms.Label RVPatientID;
        private System.Windows.Forms.Label RVPatientBirthday;
        private System.Windows.Forms.Label RVSymptom;
        private System.Windows.Forms.Label RVWorking;
        private System.Windows.Forms.Label RVSmokeDrunkBetelNut;
        private System.Windows.Forms.Label RVAdvisorName;
        private System.Windows.Forms.Button ExportWord;
    }
}

