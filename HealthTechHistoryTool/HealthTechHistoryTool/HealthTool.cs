﻿using Microsoft.Office.Interop.Word;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace HealthTechHistoryTool
{
    public partial class HealthTool : Form
    {
        public HealthTool()
        {
            InitializeComponent();
            getPatientsIndex();
        }

        string gender = "";

        string sportHabit = "";

        List<Patient> patientList;

        string[] badHabits = new string[3];

        string[] healthInfo = new string[17];

        #region Windows Form 控制項
        /// <summary>
        /// 控制畫面大小比例
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private float X;//當前窗體的寬度
        private float Y;//當前窗體的高度
        private void HealthTool_Load(object sender, EventArgs e)
        {
            X = this.Width;//獲取窗體的寬度
            Y = this.Height;//獲取窗體的高度
            setTag(this);//調用方法
        }
        private void HealthTool_Resize(object sender, EventArgs e)
        {
            float newx = (this.Width) / X; //窗體寬度縮放比例
            float newy = (this.Height) / Y;//窗體高度縮放比例
            setControls(newx, newy, this);//隨窗體改變控制項大小
        }
        private void setTag(Control cons)
        {
            foreach (Control con in cons.Controls)
            {
                con.Tag = con.Width + ":" + con.Height + ":" + con.Left + ":" + con.Top + ":" + con.Font.Size;
                if (con.Controls.Count > 0)
                    setTag(con);
            }
        }
        private void setControls(float newx, float newy, Control cons)
        {
            //遍歷窗體中的控制項，重新設置控制項的值
            foreach (Control con in cons.Controls)
            {
                if (con.Tag != null)
                {
                    var mytag = con.Tag.ToString().Split(':');//獲取控制項的Tag屬性值，並分割後存儲字元串數組
                    float a = System.Convert.ToSingle(mytag[0]) * newx;//根據窗體縮放比例確定控制項的值，寬度
                    con.Width = (int)a;//寬度
                    a = System.Convert.ToSingle(mytag[1]) * newy;//高度
                    con.Height = (int)(a);
                    a = System.Convert.ToSingle(mytag[2]) * newx;//左邊距離
                    con.Left = (int)(a);
                    a = System.Convert.ToSingle(mytag[3]) * newy;//上邊緣距離
                    con.Top = (int)(a);
                    Single currentSize = System.Convert.ToSingle(mytag[4]) * newy;//字體大小
                    con.Font = new System.Drawing.Font(con.Font.Name, currentSize, con.Font.Style, con.Font.Unit);
                    if (con.Controls.Count > 0)
                    {
                        setControls(newx, newy, con);
                    }
                }
            }
        }


        #endregion

        private void CallLoginBtn_Click(object sender, EventArgs e)
        {
            if (AdviserName.Text != "未登入")
            {
                CallLoginBtn.Text = "登入";
                AdviserName.Text = "未登入";
            }
            else
            {
                CallLoginBtn.Text = "登出";
                AdviserName.Text = "郭素君";
            }
            
        }

        private void maleRB_CheckedChanged(object sender, EventArgs e)
        {
            if (maleRB.Checked)
            {
                gender = "男";
            }
        }

        private void femaleRB_CheckedChanged(object sender, EventArgs e)
        {
            if (femaleRB.Checked)
            {
                gender = "女";
            }
        }

        private void PatientPhone_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar) || char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }//只能輸入數字


        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                checkBox2.Checked = false;
                checkBox3.Checked = false;
                checkBox4.Checked = false;
                checkBox5.Checked = false;
                checkBox6.Checked = false;
                checkBox7.Checked = false;
                checkBox8.Checked = false;
                checkBox9.Checked = false;
                checkBox10.Checked = false;
                checkBox11.Checked = false;
                checkBox12.Checked = false;
                checkBox13.Checked = false;
                checkBox14.Checked = false;
                checkBox15.Checked = false;
                checkBox16.Checked = false;
                checkBox17.Checked = false;
                checkBox18.Checked = false;

                for(int i = 0; i< healthInfo.Length; i++)
                {
                    healthInfo[i] = "";
                }
            }
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox2.Checked)
            {
                checkBox1.Checked = false;
                healthInfo[0] = "肺結核";
            }
            else
            {
                healthInfo[0] = "";
            }
        }

        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox3.Checked)
            {
                checkBox1.Checked = false;
                healthInfo[1] = "心臟病";
            }
            else
            {
                healthInfo[1] = "";
            }
        }

        private void checkBox4_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox4.Checked)
            {
                checkBox1.Checked = false;
                healthInfo[2] = "肝炎";
            }
            else
            {
                healthInfo[2] = "";
            }
        }

        private void checkBox5_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox5.Checked)
            {
                checkBox1.Checked = false;
                healthInfo[3] = "氣喘";
            }
            else
            {
                healthInfo[3] = "";
            }
        }

        private void checkBox6_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox6.Checked)
            {
                checkBox1.Checked = false;
                healthInfo[4] = "腎臟病";
            }
            else
            {
                healthInfo[4] = "";
            }
        }

        private void checkBox7_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox7.Checked)
            {
                checkBox1.Checked = false;
                healthInfo[5] = "癲癇";
            }
            else
            {
                healthInfo[5] = "";
            }
        }

        private void checkBox8_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox8.Checked)
            {
                checkBox1.Checked = false;
                healthInfo[6] = "紅斑性狼瘡";
            }
            else
            {
                healthInfo[6] = "";
            }
        }

        private void checkBox9_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox9.Checked)
            {
                checkBox1.Checked = false;
                healthInfo[7] = "血友病";
            }
            else
            {
                healthInfo[7] = "";
            }
        }

        private void checkBox10_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox10.Checked)
            {
                checkBox1.Checked = false;
                healthInfo[8] = "蠶豆症";
            }
            else
            {
                healthInfo[8] = "";
            }
        }

        private void checkBox11_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox11.Checked)
            {
                checkBox1.Checked = false;
                healthInfo[9] = "關節炎";
            }
            else
            {
                healthInfo[9] = "";
            }
        }

        private void checkBox12_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox12.Checked)
            {
                checkBox1.Checked = false;
                healthInfo[10] = "糖尿病";
            }
            else
            {
                healthInfo[10] = "";
            }
        }

        private void checkBox13_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox13.Checked)
            {
                checkBox1.Checked = false;
                cb13Text.Enabled = true;

            }
            else
            {
                cb13Text.Text = "";
                cb13Text.Enabled = false;
                healthInfo[11] = "";
            }
        }

        private void checkBox14_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox14.Checked)
            {
                checkBox1.Checked = false;
                cb14Text.Enabled = true;
                
            }
            else
            {
                cb14Text.Text = "";
                cb14Text.Enabled = false;
                healthInfo[12] = "";
            }
        }

        private void checkBox15_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox15.Checked)
            {
                checkBox1.Checked = false;
                healthInfo[13] = "海洋性貧血";
            }
            else
            {
                healthInfo[13] = "";
            }
        }

        private void checkBox16_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox16.Checked)
            {
                checkBox1.Checked = false;
                cb16Text.Enabled = true;
                
            }
            else
            {
                cb16Text.Text = "";
                cb16Text.Enabled = false;
                healthInfo[14] = "";
            }
        }

        private void checkBox17_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox17.Checked)
            {
                checkBox1.Checked = false;
                cb17Text.Enabled = true;
                
            }
            else
            {
                cb17Text.Text = "";
                cb17Text.Enabled = false;
                healthInfo[15] = "";
            }
        }

        private void checkBox18_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox18.Checked)
            {
                checkBox1.Checked = false;
                cb18Text.Enabled = true;
                
            }
            else
            {
                cb18Text.Text = "";
                cb18Text.Enabled = false;
                healthInfo[16] = "";
            }
        }

        private void SportsRadioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if (SportsRadioButton1.Checked)
            {
                sportHabit = "有";
            }
        }

        private void SportsRadioButton2_CheckedChanged(object sender, EventArgs e)
        {
            if (SportsRadioButton2.Checked)
            {
                sportHabit = "無";
            }
        }

        private void SmokeCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (SmokeCheckBox.Checked)
            {
                badHabits[0] = "抽菸";
            }
            else
            {
                badHabits[0] = "";
            }
        }

        private void DrunkCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (DrunkCheckBox.Checked)
            {
                badHabits[1] = "喝酒";
            }
            else
            {
                badHabits[1] = "";
            }
        }

        private void BetelNutCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (BetelNutCheckBox.Checked)
            {
                badHabits[2] = "檳榔";
            }
            else
            {
                badHabits[2] = "";
            }
        }

        private void InsertTreatment_Click(object sender, EventArgs e)
        {

            if (AdviserName.Text != "未登入" && !string.IsNullOrEmpty(PatientName.Text))
            {
               
                Patient patient = new Patient();

                patient.adviserName = AdviserName.Text;
                patient.patientName = PatientName.Text;
                patient.patientID = PatientID.Text;
                patient.patientPhone = PatientPhone.Text;
                patient.bloodType = BloodTypeComboBox.Text;
                patient.patientBirthday = DateTime.Parse(PatientBirthday.Text);
                patient.gender = gender;
                patient.patientAddress = PatientAddress.Text;

                patient.lifestyleResult += SleepHabitsComboBox.Text + "-";
                patient.lifestyleResult += BreakfastHabitComboBox.Text + "-";
                patient.lifestyleResult += sportHabit + "-";
                patient.lifestyleResult += badHabits[0] + "-";
                patient.lifestyleResult += badHabits[1] + "-";
                patient.lifestyleResult += badHabits[2] + "-";
                patient.lifestyleResult += BowelHabitsComboBox.Text + "-";
                patient.lifestyleResult += WorkingComboBox.Text;

                patient.healthInfoResult = "";

                healthInfo[11] = String.IsNullOrEmpty(cb13Text.Text) ? "" : "心理或精神疾病:" + cb13Text.Text;
                healthInfo[12] = String.IsNullOrEmpty(cb14Text.Text) ? "" : "癌症:" + cb14Text.Text;
                healthInfo[14] = String.IsNullOrEmpty(cb16Text.Text) ? "" : "重大手術:" + cb16Text.Text;
                healthInfo[15] = String.IsNullOrEmpty(cb17Text.Text) ? "" : "過敏物質:" + cb17Text.Text;
                healthInfo[16] = String.IsNullOrEmpty(cb18Text.Text) ? "" : "其他:" + cb18Text.Text;


                for (int i = 0; i < healthInfo.Length; i++)
                {
                    if (i < healthInfo.Length - 1)
                    {
                        patient.healthInfoResult += healthInfo[i] + "-";
                    }
                    else
                    {
                        patient.healthInfoResult += healthInfo[i];
                    }

                }

                patient.symptomText = SymptomText.Text;

                patient.firstVisitTime = DateTime.Parse(FirstVisitDateTimePicker.Text);

            //string[] temp = result.Split('-');
            //int count = 0;
            //for (int i = 0; i < temp.Length; i++)
            //{
            //    if (temp[i] != "")
            //    {
            //        count++;
            //    }
            //}



                var consString = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
                var sqlString = @"USE HealthTech
                                  INSERT INTO TreatmentHistory
                                  VALUES('" + patient.adviserName + "','" + patient.patientName + "','" + patient.patientID + "','" + patient.patientPhone + "','" + patient.bloodType + "','" + patient.patientBirthday.ToString("yyyy/MM/dd") + "','" + patient.gender + "','" + patient.patientAddress +"','" + patient.healthInfoResult + "','" + patient.lifestyleResult + "','" + patient.symptomText +"','" + patient.firstVisitTime.ToString("yyyy/MM/dd") + "')";

                try
                {
                    using (SqlConnection con = new SqlConnection(consString))
                    {
                        using (SqlCommand cmd = con.CreateCommand())
                        {
                            cmd.CommandType = CommandType.Text;
                            cmd.CommandText = sqlString;
                            cmd.Connection = con;


                            con.Open();


                            cmd.ExecuteNonQuery();

                            con.Close();


                            MessageBox.Show("匯入成功");
                        }
                    }
                }
                catch (Exception err)
                {

                MessageBox.Show(err.ToString());
                }

                getPatientsIndex();//更新病例列表

            }
            else
            {
                MessageBox.Show("未登入或未填病例名稱");
            }
        }

        //patient search
        private void getPatientsIndex()
        {
            var consString = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
            var sqlString = @"SELECT *
                              FROM [HealthTech].[dbo].[TreatmentHistory]";

            try
            {
                var resultlist = new List<Patient>();
                using (SqlConnection con = new SqlConnection(consString))
                {
                    using (SqlCommand cmd = con.CreateCommand())
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = sqlString;
                        cmd.Connection = con;

                        con.Open();

                        using (var dbreader = cmd.ExecuteReader())
                        {
                            while (dbreader.Read())
                            {
                                resultlist.Add(new Patient()
                                {
                                    adviserName = dbreader.GetValue(0).ToString(),
                                    patientName = dbreader.GetValue(1).ToString(),
                                    patientID = dbreader.GetValue(2).ToString(),
                                    patientPhone = dbreader.GetValue(3).ToString(),
                                    bloodType = dbreader.GetValue(4).ToString(),
                                    patientBirthday = Convert.ToDateTime(dbreader.GetDateTime(5).ToString("yyyy/MM/dd")),
                                    //patientBirthday = DateTime.ParseExact(dbreader.GetValue(5).ToString(), "yyyy-MM-dd", CultureInfo.InvariantCulture),
                                    gender = dbreader.GetValue(6).ToString(),
                                    patientAddress = dbreader.GetValue(7).ToString(),
                                    healthInfoResult = dbreader.GetValue(8).ToString(),
                                    lifestyleResult = dbreader.GetValue(9).ToString(),
                                    symptomText = dbreader.GetValue(10).ToString(),
                                    firstVisitTime = Convert.ToDateTime(dbreader.GetDateTime(11).ToString("yyyy/MM/dd"))
                                });
                            }
                        }

                        con.Close();

                        patientList = new List<Patient>();
                        var patientNameList = new List<string>();
                        foreach (var patient in resultlist)
                        {
                            patientList.Add(patient);
                            patientNameList.Add(patient.patientName + ", " + patient.firstVisitTime.ToString("yyyy/MM/dd"));
                        }
                        patientlistbox.DataSource = patientNameList;

                    }
                }
            }
            catch (Exception err)
            {

                MessageBox.Show(err.ToString());
            }

        }

        private void patientlistbox_SelectedIndexChanged(object sender, EventArgs e)
        {
            int index = patientlistbox.Items.IndexOf(patientlistbox.SelectedItem.ToString());
            string healthInfo = "";
            string smokedrunkbetelnutInfo = "";

            RVfirstvisitdate.Text = patientList[index].firstVisitTime.ToString("yyyy/MM/dd");
            RVPatientName.Text = patientList[index].patientName;
            RVPatientID.Text = patientList[index].patientID;
            RVBloodType.Text = patientList[index].bloodType;
            RVPatientBirthday.Text = patientList[index].patientBirthday.ToString("yyyy/MM/dd");
            RVPatientGender.Text = patientList[index].gender;
            RVPatientPhone.Text = patientList[index].patientPhone;
            RVPatientAddress.Text = patientList[index].patientAddress;

            var healthInfoList = patientList[index].healthInfoResult.Split('-');
            foreach(var i in healthInfoList)
            {
                var Infoempty = i == "" ? "" : healthInfo += i + '、';
            }
            if (healthInfo.Length > 0) healthInfo = healthInfo.Remove(healthInfo.Length - 1);
            RVHealthInfoResult.Text = healthInfo;

            var lifeStyleList = patientList[index].lifestyleResult.Split('-');
            RVSleep.Text = lifeStyleList[0];
            RVBreakfast.Text = lifeStyleList[1];
            RVSport.Text = lifeStyleList[2];
            var smokeempty = string.IsNullOrEmpty(lifeStyleList[3]) ? "" : smokedrunkbetelnutInfo += lifeStyleList[3]+'、';
            var drunkempty = string.IsNullOrEmpty(lifeStyleList[4]) ? "" : smokedrunkbetelnutInfo += lifeStyleList[4]+'、';
            var betelnutempty = string.IsNullOrEmpty(lifeStyleList[5]) ? "" : smokedrunkbetelnutInfo += lifeStyleList[5]+'、';
            if (smokedrunkbetelnutInfo.Length > 0) smokedrunkbetelnutInfo = smokedrunkbetelnutInfo.Remove(smokedrunkbetelnutInfo.Length - 1);
            
            RVSmokeDrunkBetelNut.Text = smokedrunkbetelnutInfo;
            RVBowel.Text = lifeStyleList[6];
            RVWorking.Text = lifeStyleList[7];
            RVSymptom.Text = patientList[index].symptomText;
            RVAdvisorName.Text = patientList[index].adviserName;

        }

        private void ExportWord_Click(object sender, EventArgs e)
        {
            //SaveFileDialog saveFile = new SaveFileDialog();
            //saveFile.FileName = RVPatientName.Text+".docx";
            //if (saveFile.ShowDialog() == DialogResult.OK)
            //{
            //    Stream stream = File.Open(saveFile.FileName, FileMode.CreateNew);
            //    StreamWriter streamWriter = new StreamWriter(stream);

            //    streamWriter.Write(preview.Text);
            //}
            createWordDoc();
        }

        private void createWordDoc()
        {
            object objMissing = System.Reflection.Missing.Value;
            object objEndOfDocument = "\\endofdoc";
            Microsoft.Office.Interop.Word.Application application = new Microsoft.Office.Interop.Word.Application();
            application.Visible = true;
            Microsoft.Office.Interop.Word.Document document = application.Documents.Add(ref objMissing, ref objMissing, ref objMissing, ref objMissing);
            Microsoft.Office.Interop.Word.Table table;
            Microsoft.Office.Interop.Word.Range range = document.Bookmarks.get_Item(ref objEndOfDocument).Range;
            table = document.Tables.Add(range, 17, 1, ref objMissing, ref objMissing);
            table.Cell(1, 1).Range.Text = RVfirstvisitdatelb.Text+" "+RVfirstvisitdate.Text;
            table.Cell(2, 1).Range.Text = RVPatientNamelb.Text+" "+RVPatientName.Text;
            table.Cell(3, 1).Range.Text = RVPatientIDlb.Text+ " "+RVPatientID.Text;
            table.Cell(4, 1).Range.Text = RVBloodTypelb.Text+" "+RVBloodType.Text;
            table.Cell(5, 1).Range.Text = RVPatientBirthdaylb.Text+" "+RVPatientBirthday.Text;
            table.Cell(6, 1).Range.Text = RVPatientGenderlb.Text+" "+RVPatientGender.Text;
            table.Cell(7, 1).Range.Text = RVPatientPhonelb.Text+" "+RVPatientPhone.Text;
            table.Cell(8, 1).Range.Text = RVPatientAddresslb.Text+" "+RVPatientAddress.Text;
            table.Cell(9, 1).Range.Text = RVHealthInfoResultlb.Text+" "+RVHealthInfoResult.Text;
            table.Cell(10, 1).Range.Text = RVSleeplb.Text+" "+RVSleep.Text;
            table.Cell(11, 1).Range.Text = RVBreakfastlb.Text+" "+RVBreakfast.Text;
            table.Cell(12, 1).Range.Text = RVSportlb.Text+" "+RVSport.Text;
            table.Cell(13, 1).Range.Text = RVSmokeDrunkBetelNutlb.Text+" "+RVSmokeDrunkBetelNut.Text;
            table.Cell(14, 1).Range.Text = RVBowellb.Text+" "+RVBowel.Text;
            table.Cell(15, 1).Range.Text = RVWorkinglb.Text+" "+RVWorking.Text;
            table.Cell(16, 1).Range.Text = RVSymptomlb.Text+" "+RVSymptom.Text;
            table.Cell(17, 1).Range.Text = RVAdvisorNamelb.Text+" "+RVAdvisorName.Text;
        }
    }
}
