﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthTechHistoryTool
{
    class Patient
    {
        public string adviserName { get; set; }
        public string patientName{ get; set;}
        public string patientID { get; set; }
        public string patientPhone { get; set; }
        public string bloodType { get; set; }
        public DateTime patientBirthday { get; set; }
        public string gender { get; set; }
        public string patientAddress { get; set; }

        public string healthInfoResult { get; set; }

        public string lifestyleResult { get; set; }

        public string symptomText { get; set; }

        public DateTime firstVisitTime { get; set; }
    }
}
