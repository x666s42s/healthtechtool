﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace FirstHDBlock
{
    class Program
    {
        static void Main(string[] args)
        {
            string HDContent = "神愛世人";
            string HDSender = "HDSystem";
            string HDReceiver = "HDSystem";

            string Date = DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss.fff");

            SHA256 sha256 = new SHA256CryptoServiceProvider();

            byte[] source = Encoding.Default.GetBytes(HDContent + HDSender + HDReceiver + Date);

            byte[] hash = sha256.ComputeHash(source);

            string result = Convert.ToBase64String(hash);

            Console.WriteLine(result);

            string sqlcmd = @"INSERT INTO [dbo].[HDBlocks] ([HDTransactionHash], [HDContent], [HDSender], [HDReceiver], [Date])
                              VALUES ('" + result + "', '" + HDContent + "', '" + HDSender + "', '" + HDReceiver + "', '" + Date + "');";

            string consString = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
            using (SqlConnection con = new SqlConnection(consString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Connection = con;
                    cmd.CommandText = sqlcmd;

                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }

            Console.ReadKey();
        }
    }
}
