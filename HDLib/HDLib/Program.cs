﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace HealthDaimond
{
    class HDiamond
    {
        public string HDID { get; set; }
        public string FirstOwnerID { get; set; }
        public string CurrentOwnerID { get; set; }
        public string GenerateDateTime { get; set; }
        public string TransactionDateTime { get; set; }

        public HDiamond(string HDID, string FirstOwnerID, string CurrentOwnerID, string GenerateDateTime, string TransactionDateTime)
        {
            this.HDID = HDID;
            this.FirstOwnerID = FirstOwnerID;
            this.CurrentOwnerID = CurrentOwnerID;
            this.GenerateDateTime = GenerateDateTime;
            this.TransactionDateTime = TransactionDateTime;
        }


        public static DataTable getHDaimondDataTable(string HDID, string FirstOwner, int HDCount)
        {
            SHA256 sha256 = new SHA256CryptoServiceProvider();


            List<HDiamond> HDList = new List<HDiamond>();

            for (int i = 0; i < HDCount; i++)
            {
                if (i == 0)
                {
                    byte[] source = Encoding.Default.GetBytes(HDID + FirstOwner + DateTime.Now);

                    byte[] hash = sha256.ComputeHash(source);

                    string result = Convert.ToBase64String(hash);

                    HDList.Add(new HDiamond(result, FirstOwner, FirstOwner, DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss.fff"), DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss.fff")));
                }
                else
                {
                    byte[] source = Encoding.Default.GetBytes(HDList[i - 1].HDID + FirstOwner + DateTime.Now);

                    byte[] hash = sha256.ComputeHash(source);

                    string result = Convert.ToBase64String(hash);

                    HDList.Add(new HDiamond(result, FirstOwner, FirstOwner, DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss.fff"), DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss.fff")));
                }

            }

            DataTable tempDt = new DataTable();

            tempDt.Columns.Add("HDID");
            tempDt.Columns.Add("FirstOwnerID");
            tempDt.Columns.Add("CurrentOwnerID");
            tempDt.Columns.Add("GenerateDateTime");
            tempDt.Columns.Add("TransactionDateTime");

            foreach (var HD in HDList)
            {
                DataRow newRow = tempDt.NewRow();

                newRow["HDID"] = HD.HDID;
                newRow["FirstOwnerID"] = HD.FirstOwnerID;
                newRow["CurrentOwnerID"] = HD.CurrentOwnerID;
                newRow["GenerateDateTime"] = HD.GenerateDateTime;
                newRow["TransactionDateTime"] = HD.TransactionDateTime;

                tempDt.Rows.Add(newRow);

            }

            return tempDt;

        }
    }
}
