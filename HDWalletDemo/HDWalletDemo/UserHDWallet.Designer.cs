﻿namespace HDWalletDemo
{
    partial class UserHDWallet
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserHDWallet));
            this.Hellolb = new System.Windows.Forms.Label();
            this.UserIdlb = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.Refreshbtn = new System.Windows.Forms.Button();
            this.TotalHDCountlb = new System.Windows.Forms.Label();
            this.Tatallb = new System.Windows.Forms.Label();
            this.SearchUserlb = new System.Windows.Forms.Label();
            this.SearchUserTextBox = new System.Windows.Forms.TextBox();
            this.ShowHDHash = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.Transactionbtn = new System.Windows.Forms.Button();
            this.QuantityText = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // Hellolb
            // 
            this.Hellolb.AutoSize = true;
            this.Hellolb.Font = new System.Drawing.Font("Consolas", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Hellolb.Location = new System.Drawing.Point(49, 24);
            this.Hellolb.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Hellolb.Name = "Hellolb";
            this.Hellolb.Size = new System.Drawing.Size(112, 40);
            this.Hellolb.TabIndex = 0;
            this.Hellolb.Text = "Hello";
            // 
            // UserIdlb
            // 
            this.UserIdlb.AutoSize = true;
            this.UserIdlb.Font = new System.Drawing.Font("Consolas", 26.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UserIdlb.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.UserIdlb.Location = new System.Drawing.Point(48, 64);
            this.UserIdlb.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.UserIdlb.Name = "UserIdlb";
            this.UserIdlb.Size = new System.Drawing.Size(0, 51);
            this.UserIdlb.TabIndex = 1;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.Refreshbtn);
            this.panel1.Controls.Add(this.TotalHDCountlb);
            this.panel1.Controls.Add(this.Tatallb);
            this.panel1.Controls.Add(this.UserIdlb);
            this.panel1.Controls.Add(this.Hellolb);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(613, 125);
            this.panel1.TabIndex = 2;
            // 
            // Refreshbtn
            // 
            this.Refreshbtn.Image = ((System.Drawing.Image)(resources.GetObject("Refreshbtn.Image")));
            this.Refreshbtn.Location = new System.Drawing.Point(276, 24);
            this.Refreshbtn.Margin = new System.Windows.Forms.Padding(4);
            this.Refreshbtn.Name = "Refreshbtn";
            this.Refreshbtn.Size = new System.Drawing.Size(93, 88);
            this.Refreshbtn.TabIndex = 4;
            this.Refreshbtn.UseVisualStyleBackColor = true;
            this.Refreshbtn.Click += new System.EventHandler(this.Refreshbtn_Click);
            // 
            // TotalHDCountlb
            // 
            this.TotalHDCountlb.AutoSize = true;
            this.TotalHDCountlb.Font = new System.Drawing.Font("Consolas", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TotalHDCountlb.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.TotalHDCountlb.Location = new System.Drawing.Point(377, 64);
            this.TotalHDCountlb.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.TotalHDCountlb.Name = "TotalHDCountlb";
            this.TotalHDCountlb.Size = new System.Drawing.Size(46, 51);
            this.TotalHDCountlb.TabIndex = 3;
            this.TotalHDCountlb.Text = "$";
            // 
            // Tatallb
            // 
            this.Tatallb.AutoSize = true;
            this.Tatallb.Font = new System.Drawing.Font("Consolas", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Tatallb.Location = new System.Drawing.Point(379, 24);
            this.Tatallb.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Tatallb.Name = "Tatallb";
            this.Tatallb.Size = new System.Drawing.Size(169, 40);
            this.Tatallb.TabIndex = 2;
            this.Tatallb.Text = "Total HD";
            // 
            // SearchUserlb
            // 
            this.SearchUserlb.AutoSize = true;
            this.SearchUserlb.Font = new System.Drawing.Font("Consolas", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SearchUserlb.Location = new System.Drawing.Point(52, 155);
            this.SearchUserlb.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.SearchUserlb.Name = "SearchUserlb";
            this.SearchUserlb.Size = new System.Drawing.Size(180, 32);
            this.SearchUserlb.TabIndex = 3;
            this.SearchUserlb.Text = "Search User";
            // 
            // SearchUserTextBox
            // 
            this.SearchUserTextBox.Location = new System.Drawing.Point(57, 199);
            this.SearchUserTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.SearchUserTextBox.Name = "SearchUserTextBox";
            this.SearchUserTextBox.Size = new System.Drawing.Size(211, 25);
            this.SearchUserTextBox.TabIndex = 4;
            this.SearchUserTextBox.TextChanged += new System.EventHandler(this.SearchUserTextBox_TextChanged);
            this.SearchUserTextBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.SearchUserTextBox_KeyUp);
            // 
            // ShowHDHash
            // 
            this.ShowHDHash.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ShowHDHash.Font = new System.Drawing.Font("Consolas", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ShowHDHash.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ShowHDHash.Location = new System.Drawing.Point(343, 155);
            this.ShowHDHash.Margin = new System.Windows.Forms.Padding(4);
            this.ShowHDHash.Name = "ShowHDHash";
            this.ShowHDHash.Size = new System.Drawing.Size(240, 62);
            this.ShowHDHash.TabIndex = 5;
            this.ShowHDHash.Text = "Show HD Hash";
            this.ShowHDHash.UseVisualStyleBackColor = true;
            this.ShowHDHash.Click += new System.EventHandler(this.ShowHDHash_Click);
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 15;
            this.listBox1.Location = new System.Drawing.Point(57, 234);
            this.listBox1.Margin = new System.Windows.Forms.Padding(4);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(211, 184);
            this.listBox1.TabIndex = 6;
            // 
            // Transactionbtn
            // 
            this.Transactionbtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Transactionbtn.Font = new System.Drawing.Font("Consolas", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Transactionbtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Transactionbtn.Location = new System.Drawing.Point(343, 356);
            this.Transactionbtn.Margin = new System.Windows.Forms.Padding(4);
            this.Transactionbtn.Name = "Transactionbtn";
            this.Transactionbtn.Size = new System.Drawing.Size(240, 62);
            this.Transactionbtn.TabIndex = 8;
            this.Transactionbtn.Text = "Transaction";
            this.Transactionbtn.UseVisualStyleBackColor = true;
            this.Transactionbtn.Click += new System.EventHandler(this.Transactionbtn_Click);
            // 
            // QuantityText
            // 
            this.QuantityText.Location = new System.Drawing.Point(343, 323);
            this.QuantityText.Margin = new System.Windows.Forms.Padding(4);
            this.QuantityText.Name = "QuantityText";
            this.QuantityText.Size = new System.Drawing.Size(132, 25);
            this.QuantityText.TabIndex = 9;
            this.QuantityText.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.QuantityText_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Consolas", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(340, 287);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(135, 32);
            this.label1.TabIndex = 10;
            this.label1.Text = "Quantity";
            // 
            // UserHDWallet
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(613, 444);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.QuantityText);
            this.Controls.Add(this.Transactionbtn);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.ShowHDHash);
            this.Controls.Add(this.SearchUserTextBox);
            this.Controls.Add(this.SearchUserlb);
            this.Controls.Add(this.panel1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "UserHDWallet";
            this.Text = "UserHDWallet";
            this.Load += new System.EventHandler(this.UserHDWallet_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Hellolb;
        private System.Windows.Forms.Label UserIdlb;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button Refreshbtn;
        private System.Windows.Forms.Label TotalHDCountlb;
        private System.Windows.Forms.Label Tatallb;
        private System.Windows.Forms.Label SearchUserlb;
        private System.Windows.Forms.TextBox SearchUserTextBox;
        private System.Windows.Forms.Button ShowHDHash;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Button Transactionbtn;
        private System.Windows.Forms.TextBox QuantityText;
        private System.Windows.Forms.Label label1;
    }
}