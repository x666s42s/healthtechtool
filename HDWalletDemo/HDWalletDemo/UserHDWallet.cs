﻿using HealthDaimond;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HDWalletDemo
{
    public partial class UserHDWallet : Form
    {
        private HDWallet hDWallet;

        DataTable tempUserList = null;

        private int totalHDCount = 0;

        string constr = HDWalletDemo.Properties.Settings.Default.HDDemoConnectionString;

        private string userID = "";

        HDBlock lastHDBlock = null;

        //HDBlock lastHDBlock = null;

        private void UserHDWallet_Load(object sender, EventArgs e)
        {
            RefreshLoad();
        }

        public UserHDWallet(HDWallet hDWallet, string text)
        {
            InitializeComponent();
            this.hDWallet = hDWallet;
            UserIdlb.Text = text;
            this.userID = text;
            Text = "UserHDWallet - " + text;


            string sqlStr = @"SELECT TOP 1 *
                              FROM [dbo].[HDBlocks]
						      ORDER BY Date DESC";

            using (var conn = new SqlConnection(constr))
            {

                conn.Open();

                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = sqlStr;

                    var dbReader = cmd.ExecuteReader();

                    while (dbReader.Read())
                    {
                        lastHDBlock = new HDBlock(dbReader.GetValue(0).ToString(), dbReader.GetValue(1).ToString(), dbReader.GetValue(2).ToString(), dbReader.GetValue(3).ToString(), dbReader.GetValue(4).ToString());
                    }

                }

                conn.Close();

            }

        }

        //覆寫Close()
        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);
            hDWallet.Visible = true;
        }

        private void Refreshbtn_Click(object sender, EventArgs e)
        {
            RefreshLoad();
        }

        private void SearchUserTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void ShowHDHash_Click(object sender, EventArgs e)
        {
            HDHash hash = new HDHash(userID);
            hash.ShowDialog();
        }

        private void SearchUserTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            DataView tempUserListDV = tempUserList.DefaultView;

            tempUserListDV.RowFilter = "UserID LIKE '%" + SearchUserTextBox.Text + "%'";

        }

        private void Transactionbtn_Click(object sender, EventArgs e)
        {
           
            if (listBox1.SelectedItem != null && QuantityText.Text!="")
            {
                //string content = userID + "給予" + listBox1.Text + "共" + QuantityText.Text + "個HD" + DateTime.Now;
                //string sqlStr = @" SELECT * FROM [dbo].[HDiamond] WHERE Owner ='" + userID + "'";

                //using (var conn = new SqlConnection(constr))
                //{
                //    conn.Open();
                //    using (var cmd00 = conn.CreateCommand())
                //    {
                //        cmd00.CommandText = sqlStr;

                //        var dbReader = cmd00.ExecuteReader();

                HDBlock newHDBlock = HDBlock.GenerateHDBlock(lastHDBlock, userID + "給 " + listBox1.Text + " " + QuantityText.Text + "個 HD", userID, listBox1.Text, tempUserList.Rows[listBox1.SelectedIndex][1].ToString());



                using (SqlConnection con = new SqlConnection(constr))
                {
                    using (SqlCommand cmd = new SqlCommand("HDTranscation"))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Connection = con;

                        cmd.Parameters.AddWithValue("@HDTransactionHash", newHDBlock.HDTransactionHash);
                        cmd.Parameters.AddWithValue("@HDContent", newHDBlock.HDBlockContent);
                        cmd.Parameters.AddWithValue("@Date", newHDBlock.Date);
                        cmd.Parameters.AddWithValue("@Count", QuantityText.Text);
                        cmd.Parameters.AddWithValue("@Sender", userID);
                        cmd.Parameters.AddWithValue("@Receiver", listBox1.Text);
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                            //lastHDBlock = new HDBlock(dbReader.GetValue(0).ToString(), dbReader.GetValue(1).ToString(), dbReader.GetValue(2).ToString(), dbReader.GetValue(3).ToString(), dbReader.GetValue(4).ToString());
                    //}
                    //conn.Close();
                }
                //HDBlock newHDBlock = HDBlock.GenerateHDBlock(lastHDBlock, content, userID, listBox1.Text, tempUserList.Rows[listBox1.SelectedIndex][1].ToString());
                MessageBox.Show("交易成功");
                RefreshLoad();
            }
            else
            {
                MessageBox.Show("請輸入交易金額和選擇交易人員");
            }

        }

        private void QuantityText_KeyPress(object sender, KeyPressEventArgs e)
        {
                if (Char.IsDigit(e.KeyChar) && Convert.ToInt32(QuantityText.Text + e.KeyChar) <= totalHDCount || Char.IsControl(e.KeyChar))
                {
                    e.Handled = false;
                }
                else
                {
                    e.Handled = true;
                }
           
        }

        private void RefreshLoad()
        {
            tempUserList = new DataTable();

            tempUserList.Columns.Add("UserID", typeof(string));
            tempUserList.Columns.Add("PublicKey", typeof(string));
            tempUserList.Columns.Add("HDCount", typeof(int));

            string sqlStr = @"SELECT T1.[UserID],[HDPublicKey],T2.HDCount
                              FROM [dbo].[HDKeyPair] AS T1 LEFT JOIN  [dbo].[HDWallet] AS T2
                              ON T1.UserID =T2.UserID";

            using (var conn = new SqlConnection(constr))
            {
                conn.Open();
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = sqlStr;

                    var dbReader = cmd.ExecuteReader();

                    while (dbReader.Read())
                    {
                        tempUserList.Rows.Add(dbReader.GetSqlValue(0).ToString(), dbReader.GetValue(1).ToString(), dbReader.GetValue(2));
                        if (dbReader.GetSqlValue(0).ToString() == UserIdlb.Text) totalHDCount = Convert.ToInt32(dbReader.GetValue(2));
                    }

                }

                conn.Close();

                listBox1.DataSource = tempUserList;
                listBox1.DisplayMember = tempUserList.Columns["UserID"].ToString();

                TotalHDCountlb.Text = "$" + totalHDCount;

            }
        }
    }
}
