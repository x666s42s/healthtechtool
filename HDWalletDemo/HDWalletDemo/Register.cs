﻿using HealthDaimond;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HDWalletDemo
{
    public partial class Register : Form
    {

        string HDID = "";

        string consString = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;

        public Register()
        {
            InitializeComponent();

            using (SqlConnection con = new SqlConnection(consString))
            {
                string cmdstr = @"SELECT TOP 1 [HDID]
                                  FROM [HDDemo].[dbo].[HDiamond]
                                  ORDER BY ID DESC";

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = cmdstr;
                    cmd.Connection = con;
                    con.Open();
                    using (SqlDataReader dbReader = cmd.ExecuteReader())
                    {
                        if (dbReader.Read())
                        {
                            HDID = dbReader.GetValue(0).ToString();
                        }
                       
                    }
                    con.Close();
                }
            }

        }

        private void Done_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "")
            {
                string userId = textBox1.Text;

                HealthDaimond.HDWallet wallet = HealthDaimond.HDWallet.CreateHDWallet(userId);
                HDKey keypair = HDKey.GenerateRSAKeys(userId);

                string pubkey = keypair.publicKey;
                string prikey = keypair.privateKey;

                List<HDiamond> HDList = HDiamond.GenerateHDiamond(HDID, userId, 1);


                try
                {
                    using (SqlConnection con = new SqlConnection(consString))
                    {
                        string cmdstr = @"IF (SELECT count(*) FROM HDWallet WHERE UserID = '" + userId + @"') = 0 
                                          INSERT [HDDemo].[dbo].[HDiamond] ([HDID],[Owner])
                                          VALUES ('" + HDList[0].HDID + "','" + userId + @"')
                                          INSERT [HDDemo].[dbo].[HDKeyPair] ([UserID], [HDPublicKey], [HDPrivateKey])  
                                          VALUES ('" + userId + "','" + pubkey + "','" + prikey + @"') 
                                          INSERT [HDDemo].[dbo].[HDWallet] ([UserID], [WalletHash], [HDCount])  
                                          VALUES ('" + userId + "','" + wallet.WalletHash + "','" + 1 + "')";

                        using (SqlCommand cmd = new SqlCommand())
                        {
                            cmd.CommandType = CommandType.Text;
                            cmd.CommandText = cmdstr;
                            cmd.Connection = con;
                            con.Open();
                            cmd.ExecuteNonQuery();
                            con.Close();
                        }
                    }
                    this.Close();
                }
                catch (Exception)
                {
                    MessageBox.Show("User ID已被註冊");
                }
            }  
        }
    }
}
