﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace HealthDaimond
{
    public class HDiamond
    {
        public string HDID { get; set; }
        public string Owner { get; set; }

        /// <summary>
        /// HDiamond的建構子
        /// </summary>
        /// <param name="HDID"></param>
        /// <param name="Owner">HD持有人UserID</param>
        public HDiamond(string HDID, string Owner)
        {
            this.HDID = HDID;
            this.Owner = Owner;
        }

        /// <summary>
        /// 生產HDiamond
        /// </summary>
        /// <param name="HDID">最近一組HDID</param>
        /// <param name="Owner">HD持有人的UserID</param>
        /// <param name="HDCount">此次須產生的HD量</param>
        /// <returns></returns>
        public static List<HDiamond> GenerateHDiamond(string HDID, string Owner, int HDCount)
        {
            SHA256 sha256 = new SHA256CryptoServiceProvider();


            List<HDiamond> HDList = new List<HDiamond>();

            for (int i = 0; i < HDCount; i++)
            {
                if (i == 0)
                {
                    byte[] source = Encoding.Default.GetBytes(HDID + DateTime.Now);

                    byte[] hash = sha256.ComputeHash(source);

                    string result = Convert.ToBase64String(hash);

                    HDList.Add(new HDiamond(result, Owner));
                }
                else
                {
                    byte[] source = Encoding.Default.GetBytes(HDList[i - 1].HDID + DateTime.Now);

                    byte[] hash = sha256.ComputeHash(source);

                    string result = Convert.ToBase64String(hash);

                    HDList.Add(new HDiamond(result, Owner));
                }

            }

            return HDList;

        }


    }

    public class HDBlock
    {
        public string HDTransactionHash { get; set; }
        public string HDBlockContent { get; set; }
        public string HDSender { get; set; }
        public string HDReceiver { get; set; }
        public string Date { get; set; }


        public HDBlock(string hDTransactionHash, string hDBlockContent, string hDSender, string hDReceiver, string date)
        {
            this.HDTransactionHash = hDTransactionHash;
            this.HDBlockContent = hDBlockContent;
            this.HDSender = hDSender;
            this.HDReceiver = hDReceiver;
            this.Date = date;
        }

        /// <summary>
        /// 產生交易明細
        /// </summary>
        /// <param name="lastHDBlock">上一個HDBlock的物件</param>
        /// <param name="hDBlockContent">此次交易的內容</param>
        /// <param name="hDSender">此次交易的付款人</param>
        /// <param name="hDReceiver">此次交易收款人</param>
        /// <param name="hDReceiverPublicKey">收款人的RSA公鑰</param>
        /// <returns></returns>
        public static HDBlock GenerateHDBlock( HDBlock lastHDBlock, string hDBlockContent, string hDSender, string hDReceiver ,string hDReceiverPublicKey)
        {

            SHA256 sha256 = new SHA256CryptoServiceProvider();

            byte[] source = Encoding.Default.GetBytes(lastHDBlock.HDTransactionHash + lastHDBlock.HDBlockContent + lastHDBlock.HDSender + lastHDBlock.HDReceiver + lastHDBlock.Date);

            byte[] hash = sha256.ComputeHash(source);

            string result = Convert.ToBase64String(hash);

            hDBlockContent = HDKey.RSAEncrypt(hDReceiverPublicKey, hDBlockContent);

            HDBlock tempBlock =  new HDBlock(result, hDBlockContent, hDSender, hDReceiver,  DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss.fff"));

            return tempBlock;
        }
    }

    public class HDWallet
    {
        public string UserID { get; set; }
        public string WalletHash { get; set; }
        public int HDCount { get; set; }

        public HDWallet(string UserID, string WalletHash)
        {
            this.UserID = UserID;
            this.WalletHash = WalletHash;
        }

        public HDWallet (string UserID, string WalletHash, int HDCount)
        {
            this.UserID = UserID;
            this.WalletHash = WalletHash;
            this.HDCount = HDCount;
        }

        //public static DataTable CreateHDWallet(string UserID)
        //{
        //    SHA256 sha256 = new SHA256CryptoServiceProvider();

        //    byte[] source = Encoding.Default.GetBytes(UserID);

        //    byte[] hash = sha256.ComputeHash(source);

        //    string result = Convert.ToBase64String(hash);

        //    DataTable tempDt = new DataTable();

        //    tempDt.Columns.Add("UserID");
        //    tempDt.Columns.Add("WalletHash");
        //    tempDt.Columns.Add("HDCount");

        //    DataRow newRow = tempDt.NewRow();

        //    newRow["UserID"] = UserID;
        //    newRow["WalletHash"] = result;
        //    newRow["HDCount"] = 0;

        //    tempDt.Rows.Add(newRow);

        //    return tempDt;
        //}

        public static HDWallet CreateHDWallet(string UserID)
        {
            SHA256 sha256 = new SHA256CryptoServiceProvider();

            byte[] source = Encoding.Default.GetBytes(UserID + DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss.fff"));

            byte[] hash = sha256.ComputeHash(source);

            string result = Convert.ToBase64String(hash);

            HDWallet tempWallet = new HDWallet(UserID, result);

            return tempWallet;
        }

        //public string IncreaseHD( List<string> HDIDList)
        //{

        //    string temp = "";

        //    foreach (string HDID in HDIDList)
        //    {
        //        temp += Environment.NewLine + HDID;
        //    }

        //    return temp;
        //}

        //public void DecreaseHD(int count)
        //{

        //}
    }

    public class HDKey
    {
        public string userID { get; set; }
        public string publicKey { get; set; }
        public string privateKey { get; set; }

        public HDKey (string userID, string publicKey, string privateKey)
        {
            this.userID = userID;
            this.publicKey = publicKey;
            this.privateKey = privateKey;
        }


        /// <summary>
        /// 產生使用者的公私鑰對
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        public static HDKey GenerateRSAKeys(string userID)
        {
            RSACryptoServiceProvider rsa = new RSACryptoServiceProvider(2048);

            string publicKey = rsa.ToXmlString(false);
            string privateKey = rsa.ToXmlString(true);

            HDKey result = new HDKey(userID, publicKey, privateKey);
            

            return result;
        }
        /// <summary>
        /// 用於加密content，content為HD交易內容
        /// </summary>
        /// <param name="publicKey">公鑰</param>
        /// <param name="content">明文</param>
        /// <returns></returns>
        public static string RSAEncrypt(string publicKey, string content)
        {
            RSACryptoServiceProvider rsa = new RSACryptoServiceProvider(2048);
            rsa.FromXmlString(publicKey);

            string encryptString = Convert.ToBase64String(rsa.Encrypt(Encoding.UTF8.GetBytes(content), true));

            return encryptString;
        }
        /// <summary>
        /// 用於解密content，content為HD交易內容
        /// </summary>
        /// <param name="privateKey">私鑰</param>
        /// <param name="encryptedContent">密文</param>
        /// <returns></returns>
        public static string RSADecrypt(string privateKey, string encryptedContent)
        {
            RSACryptoServiceProvider rsa = new RSACryptoServiceProvider(2048);
            rsa.FromXmlString(privateKey);

            string decryptString = Encoding.UTF8.GetString(rsa.Decrypt(Convert.FromBase64String(encryptedContent), true));

            return decryptString;
        }
    }

    
}
