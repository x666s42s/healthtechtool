﻿namespace HDManagement
{
    partial class HDManagementView
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置受控資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.SearchTextBox = new System.Windows.Forms.TextBox();
            this.Ration = new System.Windows.Forms.Button();
            this.Content = new System.Windows.Forms.TextBox();
            this.Contentlb = new System.Windows.Forms.Label();
            this.HDComboBox = new System.Windows.Forms.ComboBox();
            this.HDCountlb = new System.Windows.Forms.Label();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.refresh_btn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(48, 58);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "Search";
            // 
            // SearchTextBox
            // 
            this.SearchTextBox.Location = new System.Drawing.Point(86, 55);
            this.SearchTextBox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.SearchTextBox.Name = "SearchTextBox";
            this.SearchTextBox.Size = new System.Drawing.Size(104, 22);
            this.SearchTextBox.TabIndex = 2;
            this.SearchTextBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.SearchTextBox_KeyUp);
            // 
            // Ration
            // 
            this.Ration.Location = new System.Drawing.Point(298, 296);
            this.Ration.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Ration.Name = "Ration";
            this.Ration.Size = new System.Drawing.Size(65, 27);
            this.Ration.TabIndex = 3;
            this.Ration.Text = "配幣";
            this.Ration.UseVisualStyleBackColor = true;
            this.Ration.Click += new System.EventHandler(this.Ration_Click);
            // 
            // Content
            // 
            this.Content.Location = new System.Drawing.Point(224, 114);
            this.Content.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Content.Multiline = true;
            this.Content.Name = "Content";
            this.Content.Size = new System.Drawing.Size(140, 166);
            this.Content.TabIndex = 4;
            // 
            // Contentlb
            // 
            this.Contentlb.AutoSize = true;
            this.Contentlb.Location = new System.Drawing.Point(222, 92);
            this.Contentlb.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Contentlb.Name = "Contentlb";
            this.Contentlb.Size = new System.Drawing.Size(42, 12);
            this.Contentlb.TabIndex = 5;
            this.Contentlb.Text = "Content";
            // 
            // HDComboBox
            // 
            this.HDComboBox.FormattingEnabled = true;
            this.HDComboBox.Items.AddRange(new object[] {
            "5",
            "10",
            "20",
            "50",
            "100"});
            this.HDComboBox.Location = new System.Drawing.Point(272, 58);
            this.HDComboBox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.HDComboBox.Name = "HDComboBox";
            this.HDComboBox.Size = new System.Drawing.Size(92, 20);
            this.HDComboBox.TabIndex = 6;
            this.HDComboBox.Text = "10";
            this.HDComboBox.SelectedIndexChanged += new System.EventHandler(this.HDComboBox_SelectedIndexChanged);
            this.HDComboBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.HDComboBox_KeyUp);
            // 
            // HDCountlb
            // 
            this.HDCountlb.AutoSize = true;
            this.HDCountlb.Location = new System.Drawing.Point(222, 59);
            this.HDCountlb.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.HDCountlb.Name = "HDCountlb";
            this.HDCountlb.Size = new System.Drawing.Size(50, 12);
            this.HDCountlb.TabIndex = 7;
            this.HDCountlb.Text = "HDCount";
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 12;
            this.listBox1.Location = new System.Drawing.Point(50, 114);
            this.listBox1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(140, 208);
            this.listBox1.TabIndex = 8;
            this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // refresh_btn
            // 
            this.refresh_btn.Location = new System.Drawing.Point(224, 296);
            this.refresh_btn.Name = "refresh_btn";
            this.refresh_btn.Size = new System.Drawing.Size(69, 27);
            this.refresh_btn.TabIndex = 9;
            this.refresh_btn.Text = "refresh";
            this.refresh_btn.UseVisualStyleBackColor = true;
            this.refresh_btn.Click += new System.EventHandler(this.refresh_btn_Click);
            // 
            // HDManagementView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(398, 360);
            this.Controls.Add(this.refresh_btn);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.HDCountlb);
            this.Controls.Add(this.HDComboBox);
            this.Controls.Add(this.Contentlb);
            this.Controls.Add(this.Content);
            this.Controls.Add(this.Ration);
            this.Controls.Add(this.SearchTextBox);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "HDManagementView";
            this.Text = "HDManagement";
            this.Load += new System.EventHandler(this.HDManagementView_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox SearchTextBox;
        private System.Windows.Forms.Button Ration;
        private System.Windows.Forms.TextBox Content;
        private System.Windows.Forms.Label Contentlb;
        private System.Windows.Forms.ComboBox HDComboBox;
        private System.Windows.Forms.Label HDCountlb;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Button refresh_btn;
    }
}

