﻿using HealthDaimond;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HDManagement
{

    public partial class HDManagementView : Form
    {

        DataTable tempUserList = null;

        string constr = HDManagement.Properties.Settings.Default.HDDemoConnectionString;

        string HDID = "";

        HDBlock lastHDBlock = null;

        public HDManagementView()
        {
            InitializeComponent();
        }

        private void HDManagementView_Load(object sender, EventArgs e)
        {

            DataLoad();
        }

        private void HDComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox1.SelectedItem != null)
                Content.Text = "HDSystem派發" + HDComboBox.Text + "個HD" + Environment.NewLine + "給 " + listBox1.Text + Environment.NewLine + "於" + DateTime.Now;
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (HDComboBox.Text != "")
                Content.Text = "HDSystem派發" + HDComboBox.Text + "個HD" + Environment.NewLine + "給 " + listBox1.Text + Environment.NewLine + "於" + DateTime.Now;
        }

        private void HDComboBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (listBox1.SelectedItem != null)
                Content.Text = "HDSystem派發" + HDComboBox.Text + "個HD" + Environment.NewLine + "給 " + listBox1.Text + Environment.NewLine + "於" + DateTime.Now;
        }

        private void Ration_Click(object sender, EventArgs e)
        {

            List<HDiamond> newHDList =  HDiamond.GenerateHDiamond(HDID, listBox1.Text, int.Parse(HDComboBox.Text));

            HDBlock newHDBlock = HDBlock.GenerateHDBlock(lastHDBlock, Content.Text, "HDSystem", listBox1.Text, tempUserList.Rows[listBox1.SelectedIndex][1].ToString());


            DataTable tempDt = new DataTable();

            tempDt.Columns.Add("HDID");
            tempDt.Columns.Add("Owner");

            foreach (HealthDaimond.HDiamond item in newHDList)
            {
                DataRow dr = tempDt.NewRow();
                dr["HDID"] = item.HDID;
                dr["Owner"] = item.Owner;

                tempDt.Rows.Add(dr);
            }


            if (tempDt.Rows.Count > 0)
            {

                using (SqlConnection con = new SqlConnection(constr))
                {
                    using (SqlCommand cmd = new SqlCommand("Insert_HDiamond"))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Connection = con;
                        cmd.Parameters.AddWithValue("@insHDiamondDt", tempDt);
                        cmd.Parameters.AddWithValue("@newHDTransactionHash", newHDBlock.HDTransactionHash);
                        cmd.Parameters.AddWithValue("@newHDContent", newHDBlock.HDBlockContent);
                        cmd.Parameters.AddWithValue("@sender", newHDBlock.HDSender);
                        cmd.Parameters.AddWithValue("@receiver", newHDBlock.HDReceiver);
                        cmd.Parameters.AddWithValue("@date", newHDBlock.Date);
                        cmd.Parameters.AddWithValue("@count", int.Parse(HDComboBox.Text));
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                }

                MessageBox.Show("派發成功");

                DataLoad();

            }//insert Data

            //MessageBox.Show(tempUserList.Rows[listBox1.SelectedIndex][1].ToString());

            //MessageBox.Show(HDID +"\n" + listBox1.Text + "\n"+ int.Parse(HDComboBox.Text));

            //MessageBox.Show(lastHDBlock.HDTransactionHash + "\n" + lastHDBlock.HDBlockContent + "\n" + lastHDBlock.HDSender + "\n" + lastHDBlock.HDReceiver + "\n" + lastHDBlock.Date);

        }

        

        private void SearchTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            DataView tempUserListDV = tempUserList.DefaultView;

            tempUserListDV.RowFilter = "UserID LIKE '%" + SearchTextBox.Text + "%'";


        }


        private void DataLoad()
        {
            tempUserList = new DataTable();

            tempUserList.Columns.Add("UserID", typeof(string));
            tempUserList.Columns.Add("PublicKey", typeof(string));

            string sqlStr = @"SELECT [UserID],[HDPublicKey]
                              FROM [dbo].[HDKeyPair];

                              SELECT TOP 1 [HDID]
                              FROM [HDDemo].[dbo].[HDiamond]
                              ORDER BY ID DESC;

                              SELECT TOP 1 *
                              FROM [dbo].[HDBlocks]
					          ORDER BY Date DESC;";


            using (var conn = new SqlConnection(constr))
            {
                conn.Open();
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = sqlStr;

                    var dbReader = cmd.ExecuteReader();

                    int i = 0;
                    do
                    {
                        switch (i)
                        {
                            case 0:
                                while (dbReader.Read())
                                {
                                    tempUserList.Rows.Add(dbReader.GetSqlValue(0).ToString(), dbReader.GetValue(1).ToString());
                                }
                                break;

                            case 1:
                                while (dbReader.Read())
                                {
                                    HDID = dbReader.GetValue(0).ToString();
                                }
                                break;

                            case 2:
                                while (dbReader.Read())
                                {
                                    lastHDBlock = new HDBlock(dbReader.GetValue(0).ToString(), dbReader.GetValue(1).ToString(), dbReader.GetValue(2).ToString(), dbReader.GetValue(3).ToString(), dbReader.GetValue(4).ToString());
                                }
                                break;
                        }

                        i++;

                    } while (dbReader.NextResult());
                    

                    

                    

                    
                }

                conn.Close();

                listBox1.DataSource = tempUserList;
                listBox1.DisplayMember = tempUserList.Columns["UserID"].ToString();

            }

            //sqlStr = @"";

            //using (var conn2 = new SqlConnection(constr))
            //{

            //    conn2.Open();

            //    using (var cmd2 = conn2.CreateCommand())
            //    {
            //        cmd2.CommandText = sqlStr;

            //        var dbReader = cmd2.ExecuteReader();

                    

            //    }

            //    conn2.Close();

            //}

            //sqlStr = @"";

            //using (var conn3 = new SqlConnection(constr))
            //{

            //    conn3.Open();

            //    using (var cmd3 = conn3.CreateCommand())
            //    {
            //        cmd3.CommandText = sqlStr;

            //        var dbReader = cmd3.ExecuteReader();

                    

            //    }

            //    conn3.Close();

            //}
        }

        private void refresh_btn_Click(object sender, EventArgs e)
        {
            DataLoad();
        }
    }
}
